@echo off
echo make clean ...

::Debug folder
rd /s /q .\Debug

::Release\Exe\*.out
del /f /s .\Release\Exe\*.out

::Release\Obj folder
rd /s /q .\Release\Obj

::settings folder
rd /s /q .\settings

::project file
del /f /s .\*.dep
del /f /s .\*.ewd
del /f /s .\*.ewt

echo exit ...
exit
