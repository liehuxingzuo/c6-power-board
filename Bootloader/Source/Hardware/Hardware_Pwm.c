/*******************************************************************************
  * 文件：Hardware_Pwm.c
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：PWM
*******************************************************************************/

/* 头文件 *********************************************************************/
#include "Hardware_Pwm.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量定义 *******************************************************************/
/* 函数声明 *******************************************************************/
/* 函数定义 *******************************************************************/

/*******************************************************************************
  * 函数名：Hardware_InitPwm
  * 功  能：初始化
  * 参  数：无
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void Hardware_InitPwm(void)
{
}

/***************************** 文件结束 ***************************************/
