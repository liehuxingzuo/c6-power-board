/*******************************************************************************
  * 文件：Hardware_System.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：系统
*******************************************************************************/
#ifndef __HARDWARE_SYSTEM_H
#define __HARDWARE_SYSTEM_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include <ior5f100le.h>
#include <ior5f100le_ext.h>
#include <intrinsics.h>

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
void Hardware_InitSystem(void);          // 初始化
void Hardware_SystemReset(void);         // 系统复位
void Hardware_DisableInterrupt(void);    // 禁止中断
void Hardware_EnableInterrupt(void);     // 使能中断
void Hardware_EnterCritical(void);       // 进入临界区
void Hardware_ExitCritical(void);        // 退出临界区

#endif /* __HARDWARE_SYSTEM_H */

/***************************** 文件结束 ***************************************/
