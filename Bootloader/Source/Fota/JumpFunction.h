/*******************************************************************************
  * 文件：JumpFuction.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：跳转功能
*******************************************************************************/
#ifndef __JUMP_FUNCTION_H
#define __JUMP_FUNCTION_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "ImageHeader.h"

/* 宏定义 *********************************************************************/
#define u16IMAGE_CRC_SEED    ((U16) 0x1021)    // 程序校验初始值

/* 类型定义 *******************************************************************/
typedef void (*InterruptRoutine_tpf)(void);    // 中断函数

/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
Bool UsingBootloaderIntActive(void);             // 使用引导程序中断
Bool VerifyImage(ImageType_te eImage);            // 校验程序
void CheckBootAndJump(void);                     // 检查启动并跳转
void ApplicationInterruptHandler(U8 u8Index);    // 应用程序中断处理
void JumpToBootloader(void);                     // 跳转到引导程序
void JumpToApplication(void);                    // 跳转到应用程序

#endif /* #ifndef __JUMP_FUNCTION_H */

/***************************** 文件结束 ***************************************/
