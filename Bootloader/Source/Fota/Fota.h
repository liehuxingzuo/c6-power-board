/*******************************************************************************
  * 文件：Fota.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2019-06-21
  * 说明：固件升级
*******************************************************************************/
#ifndef __FOTA_H
#define __FOTA_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "ImageHeader.h"

/* 宏定义 *********************************************************************/
#define u8FOTA_DEFAULT_WRITE_BYTE_LEN    ((U8)   128)    // 默认写入字节长度
#define u16FOTA_FLASH_BLOCK_SIZE         ((U16) 1024)    // Flash块大小

/* 类型定义 *******************************************************************/
// 升级状态
typedef enum
{
    eFOTA_STATE_READY,           // 准备
    eFOTA_STATE_VERIFY_INFO,     // 校验信息
    eFOTA_STATE_WRITE_IMAGE,     // 写入程序
    eFOTA_STATE_COMPLETED,       // 升级完成
} FotaState_te;

// 升级控制
typedef struct
{
    FotaState_te eState;    // 升级状态
    U32 u32ImageAddress;    // 程序地址
    U32 u32ImageSize;       // 程序大小
    U32 u32WriteOffset;     // 写偏移
} FotaControl_ts;

// 校验结果
typedef enum
{
    eFOTA_VERIFY_RESULT_ACCEPT,    // 接受
    eFOTA_VERIFY_RESULT_REFUSE,    // 拒绝
    eFOTA_VERIFY_RESULT_NONEED,    // 不需要
} FotaVerifyResult_te;

/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
void Fota_Init(void);                                               // 初始化
void Fota_SetState(FotaState_te eState);                            // 设置状态
FotaState_te Fota_GetState(void);                                   // 获取状态
FotaVerifyResult_te Fota_VerifyImageInfo(ImageHeader_ts *psHeader); // 校验程序信息
void Fota_PrepareUpdate(U32 u32Address, U32 u32Size);               // 升级准备
Bool Fota_GetImageDataPara(U32 *pu32Offset, U8 *pu8Len);            // 获取程序数据参数
Bool Fota_UpdateImageData(U32 u32Offset, U8 *pu8Data, U8 u8Len);    // 更新程序数据

#endif /* __FOTA_H */

/***************************** 文件结束 ***************************************/
