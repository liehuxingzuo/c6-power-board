/*******************************************************************************
  * 文件：InterruptHandlers.c
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：中断处理
*******************************************************************************/

/* 头文件 *********************************************************************/
#include "JumpFunction.h"
#include "OS_Kernel.h"
#include "UartComm.h"
#include "Hardware.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量定义 *******************************************************************/
/* 函数声明 *******************************************************************/
/* 函数定义 *******************************************************************/

// 0x00 是复位中断地址
// 0x02是调试中断地址

/*******************************************************************************
  * 函数名：Vect_04
  * 功  能：中断函数04
  * 参  数：无
  * 返回值：无
  * 说  明：INTWDTI
*******************************************************************************/
#pragma vector =  0x04
__interrupt static void Vect_04(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x04);
    }
}

/*******************************************************************************
  * 函数名：Vect_06
  * 功  能：中断函数06
  * 参  数：无
  * 返回值：无
  * 说  明：INTLVI
*******************************************************************************/
#pragma vector = 0x06
__interrupt static void Vect_06(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x06);
    }
}

/*******************************************************************************
  * 函数名：Vect_08
  * 功  能：中断函数08
  * 参  数：无
  * 返回值：无
  * 说  明：INTP0
*******************************************************************************/
#pragma vector = 0x08
__interrupt static void Vect_08(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x08);
    }
}

/*******************************************************************************
  * 函数名：Vect_0A
  * 功  能：中断函数0A
  * 参  数：无
  * 返回值：无
  * 说  明：INTP1
*******************************************************************************/
#pragma vector = 0x0A
__interrupt static void Vect_0A(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x0A);
    }
}

/*******************************************************************************
  * 函数名：Vect_0C
  * 功  能：中断函数0C
  * 参  数：无
  * 返回值：无
  * 说  明：INTP2
*******************************************************************************/
#pragma vector = 0x0C
__interrupt static void Vect_0C(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x0C);
    }
}

/*******************************************************************************
  * 函数名：Vect_0E
  * 功  能：中断函数0E
  * 参  数：无
  * 返回值：无
  * 说  明：INTP3
*******************************************************************************/
#pragma vector = 0x0E
__interrupt static void Vect_0E(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x0E);
    }
}

/*******************************************************************************
  * 函数名：Vect_10
  * 功  能：中断函数10
  * 参  数：无
  * 返回值：无
  * 说  明：INTP4
*******************************************************************************/
#pragma vector = 0x10
__interrupt static void Vect_10(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x10);
    }
}

/*******************************************************************************
  * 函数名：Vect_12
  * 功  能：中断函数12
  * 参  数：无
  * 返回值：无
  * 说  明：INTP5
*******************************************************************************/
#pragma vector = 0x12
__interrupt static void Vect_12(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x12);
    }
}

/*******************************************************************************
  * 函数名：Vect_14
  * 功  能：中断函数14
  * 参  数：无
  * 返回值：无
  * 说  明：INTST2, INTCSI20, INTIIC20
*******************************************************************************/
#pragma vector = 0x14
__interrupt static void Vect_14(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x14);
    }
}

/*******************************************************************************
  * 函数名：Vect_16
  * 功  能：中断函数16
  * 参  数：无
  * 返回值：无
  * 说  明：INTSR2, INTCSI21, INTIIC21
*******************************************************************************/
#pragma vector = 0x16
__interrupt static void Vect_16(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x16);
    }
}

/*******************************************************************************
  * 函数名：Vect_18
  * 功  能：中断函数18
  * 参  数：无
  * 返回值：无
  * 说  明：INTSRE2, INTTM11H
*******************************************************************************/
#pragma vector = 0x18
__interrupt static void Vect_18(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x18);
    }
}

/*******************************************************************************
  * 函数名：Vect_1A
  * 功  能：中断函数1A
  * 参  数：无
  * 返回值：无
  * 说  明：INTDMA0
*******************************************************************************/
#pragma vector = 0x1A
__interrupt static void Vect_1A(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x1A);
    }
}

/*******************************************************************************
  * 函数名：Vect_1C
  * 功  能：中断函数1C
  * 参  数：无
  * 返回值：无
  * 说  明：INTDMA1
*******************************************************************************/
#pragma vector = 0x1C
__interrupt static void Vect_1C(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x1C);
    }
}

/*******************************************************************************
  * 函数名：Vect_1E
  * 功  能：中断函数1E
  * 参  数：无
  * 返回值：无
  * 说  明：INTST0, INTCSI00, INTIIC00
*******************************************************************************/
#pragma vector = 0x1E
__interrupt static void Vect_1E(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x1E);
    }
}

/*******************************************************************************
  * 函数名：Vect_20
  * 功  能：中断函数20
  * 参  数：无
  * 返回值：无
  * 说  明：INTSR0, INTCSI01, INTIIC01
*******************************************************************************/
#pragma vector = 0x20
__interrupt static void Vect_20(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x20);
    }
}

/*******************************************************************************
  * 函数名：Vect_22
  * 功  能：中断函数22
  * 参  数：无
  * 返回值：无
  * 说  明：INTSRE0, INTTM01H
*******************************************************************************/
#pragma vector = 0x22
__interrupt static void Vect_22(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x22);
    }
}

/*******************************************************************************
  * 函数名：Vect_24
  * 功  能：中断函数24
  * 参  数：无
  * 返回值：无
  * 说  明：INTST1, INTCSI10, INTIIC10
*******************************************************************************/
#pragma vector = 0x24
__interrupt static void Vect_24(void)
{
    if(UsingBootloaderIntActive())
    {
        // 发送字节处理
        UartComm_SendByteHandler(ePORT_DISPLAY_BOARD);
    }
    else
    {
        ApplicationInterruptHandler(0x24);
    }
}

/*******************************************************************************
  * 函数名：Vect_26
  * 功  能：中断函数26
  * 参  数：无
  * 返回值：无
  * 说  明：INTSR1, INTCSI11, INTIIC11
*******************************************************************************/
#pragma vector = 0x26
__interrupt static void Vect_26(void)
{
    if(UsingBootloaderIntActive())
    {
        // 接收字节处理
        UartComm_RecvByteHandler(ePORT_DISPLAY_BOARD);
    }
    else
    {
        ApplicationInterruptHandler(0x26);
    }
}

/*******************************************************************************
  * 函数名：Vect_28
  * 功  能：中断函数28
  * 参  数：无
  * 返回值：无
  * 说  明：INTSRE1, INTTM03H
*******************************************************************************/
#pragma vector = 0x28
__interrupt static void Vect_28(void)
{
    if(UsingBootloaderIntActive())
    {
        // 清除接收错误
        Hardware_ClearCommRecvError();
    }
    else
    {
        ApplicationInterruptHandler(0x28);
    }
}

/*******************************************************************************
  * 函数名：Vect_2A
  * 功  能：中断函数2A
  * 参  数：无
  * 返回值：无
  * 说  明：INTIICA0
*******************************************************************************/
#pragma vector = 0x2A
__interrupt static void Vect_2A(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x2A);
    }
}

/*******************************************************************************
  * 函数名：Vect_2C
  * 功  能：中断函数2C
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM00
*******************************************************************************/
#pragma vector = 0x2C
__interrupt static void Vect_2C(void)
{
    if(UsingBootloaderIntActive())
    {
        // 更新系统节拍
        OS_UpdateSysTick();
    }
    else
    {
        ApplicationInterruptHandler(0x2C);
    }
}

/*******************************************************************************
  * 函数名：Vect_2E
  * 功  能：中断函数2E
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM01
*******************************************************************************/
#pragma vector = 0x2E
__interrupt static void Vect_2E(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x2E);
    }
}

/*******************************************************************************
  * 函数名：Vect_30
  * 功  能：中断函数30
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM02
*******************************************************************************/
#pragma vector = 0x30
__interrupt static void Vect_30(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x30);
    }
}

/*******************************************************************************
  * 函数名：Vect_32
  * 功  能：中断函数32
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM03
*******************************************************************************/
#pragma vector = 0x32
__interrupt static void Vect_32(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x32);
    }
}

/*******************************************************************************
  * 函数名：Vect_34
  * 功  能：中断函数34
  * 参  数：无
  * 返回值：无
  * 说  明：INTAD
*******************************************************************************/
#pragma vector = 0x34
__interrupt static void Vect_34(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x34);
    }
}

/*******************************************************************************
  * 函数名：Vect_36
  * 功  能：中断函数36
  * 参  数：无
  * 返回值：无
  * 说  明：INTRTC
*******************************************************************************/
#pragma vector = 0x36
__interrupt static void Vect_36(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x36);
    }
}

/*******************************************************************************
  * 函数名：Vect_38
  * 功  能：中断函数38
  * 参  数：无
  * 返回值：无
  * 说  明：INTIT
*******************************************************************************/
#pragma vector = 0x38
__interrupt static void Vect_38(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x38);
    }
}

/*******************************************************************************
  * 函数名：Vect_3A
  * 功  能：中断函数3A
  * 参  数：无
  * 返回值：无
  * 说  明：INTKR
*******************************************************************************/
#pragma vector = 0x3A
__interrupt static void Vect_3A(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x3A);
    }
}

/*******************************************************************************
  * 函数名：Vect_3C
  * 功  能：中断函数3C
  * 参  数：无
  * 返回值：无
  * 说  明：INTST3, INTCSI30, INTIIC30
*******************************************************************************/
#pragma vector = 0x3C
__interrupt static void Vect_3C(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x3C);
    }
}

/*******************************************************************************
  * 函数名：Vect_3E
  * 功  能：中断函数3E
  * 参  数：无
  * 返回值：无
  * 说  明：INTSR3, INTCSI31, INTIIC31
*******************************************************************************/
#pragma vector = 0x3E
__interrupt static void Vect_3E(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x3E);
    }
}

/*******************************************************************************
  * 函数名：Vect_40
  * 功  能：中断函数40
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM13
*******************************************************************************/
#pragma vector = 0x40
__interrupt static void Vect_40(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x40);
    }
}

/*******************************************************************************
  * 函数名：Vect_42
  * 功  能：中断函数42
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM04
*******************************************************************************/
#pragma vector = 0x42
__interrupt static void Vect_42(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x42);
    }
}

/*******************************************************************************
  * 函数名：Vect_44
  * 功  能：中断函数44
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM05
*******************************************************************************/
#pragma vector = 0x44
__interrupt static void Vect_44(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x44);
    }
}

/*******************************************************************************
  * 函数名：Vect_46
  * 功  能：中断函数46
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM06
*******************************************************************************/
#pragma vector = 0x46
__interrupt static void Vect_46(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x46);
    }
}

/*******************************************************************************
  * 函数名：Vect_48
  * 功  能：中断函数48
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM07
*******************************************************************************/
#pragma vector = 0x48
__interrupt static void Vect_48(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x48);
    }
}

/*******************************************************************************
  * 函数名：Vect_4A
  * 功  能：中断函数4A
  * 参  数：无
  * 返回值：无
  * 说  明：INTP6
*******************************************************************************/
#pragma vector = 0x4A
__interrupt static void Vect_4A(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x4A);
    }
}

/*******************************************************************************
  * 函数名：Vect_4C
  * 功  能：中断函数4C
  * 参  数：无
  * 返回值：无
  * 说  明：INTP7
*******************************************************************************/
#pragma vector = 0x4C
__interrupt static void Vect_4C(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x4C);
    }
}

/*******************************************************************************
  * 函数名：Vect_4E
  * 功  能：中断函数4E
  * 参  数：无
  * 返回值：无
  * 说  明：INTP8
*******************************************************************************/
#pragma vector = 0x4E
__interrupt static void Vect_4E(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x4E);
    }
}

/*******************************************************************************
  * 函数名：Vect_50
  * 功  能：中断函数50
  * 参  数：无
  * 返回值：无
  * 说  明：INTP9
*******************************************************************************/
#pragma vector = 0x50
__interrupt static void Vect_50(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x50);
    }
}

/*******************************************************************************
  * 函数名：Vect_52
  * 功  能：中断函数52
  * 参  数：无
  * 返回值：无
  * 说  明：INTP10
*******************************************************************************/
#pragma vector = 0x52
__interrupt static void Vect_52(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x52);
    }
}

/*******************************************************************************
  * 函数名：Vect_54
  * 功  能：中断函数54
  * 参  数：无
  * 返回值：无
  * 说  明：INTP11
*******************************************************************************/
#pragma vector = 0x54
__interrupt static void Vect_54(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x54);
    }
}

/*******************************************************************************
  * 函数名：Vect_56
  * 功  能：中断函数56
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM10
*******************************************************************************/
#pragma vector = 0x56
__interrupt static void Vect_56(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x56);
    }
}

/*******************************************************************************
  * 函数名：Vect_58
  * 功  能：中断函数58
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM11
*******************************************************************************/
#pragma vector = 0x58
__interrupt static void Vect_58(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x58);
    }
}

/*******************************************************************************
  * 函数名：Vect_5A
  * 功  能：中断函数5A
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM12
*******************************************************************************/
#pragma vector = 0x5A
__interrupt static void Vect_5A(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x5A);
    }
}

/*******************************************************************************
  * 函数名：Vect_5C
  * 功  能：中断函数5C
  * 参  数：无
  * 返回值：无
  * 说  明：INTSRE3, INTTM13H
*******************************************************************************/
#pragma vector = 0x5C
__interrupt static void Vect_5C(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x5C);
    }
}

/*******************************************************************************
  * 函数名：Vect_5E
  * 功  能：中断函数5E
  * 参  数：无
  * 返回值：无
  * 说  明：INTMD
*******************************************************************************/
#pragma vector = 0x5E
__interrupt static void Vect_5E(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x5E);
    }
}

/*******************************************************************************
  * 函数名：Vect_60
  * 功  能：中断函数60
  * 参  数：无
  * 返回值：无
  * 说  明：INTIICA1
*******************************************************************************/
#pragma vector = 0x60
__interrupt static void Vect_60(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x60);
    }
}

/*******************************************************************************
  * 函数名：Vect_62
  * 功  能：中断函数62
  * 参  数：无
  * 返回值：无
  * 说  明：INTFL
*******************************************************************************/
#pragma vector = 0x62
__interrupt static void Vect_62(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x62);
    }
}

/*******************************************************************************
  * 函数名：Vect_64
  * 功  能：中断函数64
  * 参  数：无
  * 返回值：无
  * 说  明：INTDMA2
*******************************************************************************/
#pragma vector = 0x64
__interrupt static void Vect_64(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x64);
    }
}

/*******************************************************************************
  * 函数名：Vect_66
  * 功  能：中断函数66
  * 参  数：无
  * 返回值：无
  * 说  明：INTDMA3
*******************************************************************************/
#pragma vector = 0x66
__interrupt static void Vect_66(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x66);
    }
}

/*******************************************************************************
  * 函数名：Vect_68
  * 功  能：中断函数68
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM14
*******************************************************************************/
#pragma vector = 0x68
__interrupt static void Vect_68(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x68);
    }
}

/*******************************************************************************
  * 函数名：Vect_6A
  * 功  能：中断函数6A
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM15
*******************************************************************************/
#pragma vector = 0x6A
__interrupt static void Vect_6A(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x6A);
    }
}

/*******************************************************************************
  * 函数名：Vect_6C
  * 功  能：中断函数6C
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM16
*******************************************************************************/
#pragma vector = 0x6C
__interrupt static void Vect_6C(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x6C);
    }
}

/*******************************************************************************
  * 函数名：Vect_6E
  * 功  能：中断函数6E
  * 参  数：无
  * 返回值：无
  * 说  明：INTTM17
*******************************************************************************/
#pragma vector = 0x6E
__interrupt static void Vect_6E(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x6E);
    }
}

/*******************************************************************************
  * 函数名：Vect_7E
  * 功  能：中断函数7E
  * 参  数：无
  * 返回值：无
  * 说  明：BRK
*******************************************************************************/
#pragma vector = 0x7E
__interrupt static void Vect_7E(void)
{
    if(UsingBootloaderIntActive())
    {
        ;
    }
    else
    {
        ApplicationInterruptHandler(0x7E);
    }
}


/***************************** 文件结束 ***************************************/
