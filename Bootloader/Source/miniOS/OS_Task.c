/*******************************************************************************
  * 文件：OS_Task.c
  * 作者：zyz
  * 版本：v2.0.1
  * 日期：2020-03-24
  * 说明：任务
*******************************************************************************/

/* 头文件 *********************************************************************/
#include "OS_Task.h"
#include "Queue.h"
#include "Hardware.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量定义 *******************************************************************/
// 任务队列
static Queue_ts sTaskQueue;
// 任务缓冲区
static Task_ts asTaskBuffer[u8TASK_QUEUE_SIZE];

/* 函数声明 *******************************************************************/
/* 函数定义 *******************************************************************/

/*******************************************************************************
  * 函数名：OS_TaskInit
  * 功  能：初始化
  * 参  数：无
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void OS_TaskInit(void)
{
    // 清空任务缓冲区
    memset(asTaskBuffer, (int)NULL, sizeof(asTaskBuffer));

    // 初始化任务队列
    Queue_Init(&sTaskQueue, asTaskBuffer, u8TASK_QUEUE_SIZE, sizeof(Task_ts));
}

/*******************************************************************************
  * 函数名：OS_TaskAdd
  * 功  能：添加单次任务
  * 参  数：TaskHandler_tpf pfHandler - 处理函数
  * 返回值：结果
  * 说  明：无
*******************************************************************************/
Bool OS_TaskAdd(TaskHandler_tpf pfHandler)
{
    Bool bResult;
    Task_ts sTask;

    // 设置任务参数
    sTask.pfHandler    = (TaskHandlerEx_tpf)pfHandler;
    sTask.u32Parameter = 0;
    sTask.eRunType     = eTASK_RUN_ONCE;

    // 添加任务到任务队列
    Hardware_EnterCritical();    // 进入临界区
    bResult = Queue_Add(&sTaskQueue, &sTask);
    Hardware_ExitCritical();     // 退出临界区

    // 返回结果
    return bResult;
}

/*******************************************************************************
  * 函数名：OS_TaskCreate
  * 功  能：创建永久任务
  * 参  数：TaskHandler_tpf pfHandler - 处理函数
  * 返回值：结果
  * 说  明：无
*******************************************************************************/
Bool OS_TaskCreate(TaskHandler_tpf pfHandler)
{
    Bool bResult;
    Task_ts sTask;

    // 设置任务参数
    sTask.pfHandler    = (TaskHandlerEx_tpf)pfHandler;
    sTask.u32Parameter = 0;
    sTask.eRunType     = eTASK_RUN_ALWAYS;

    // 添加任务到任务队列
    Hardware_EnterCritical();    // 进入临界区
    bResult = Queue_Add(&sTaskQueue, &sTask);
    Hardware_ExitCritical();     // 退出临界区

    // 返回结果
    return bResult;
}

/*******************************************************************************
  * 函数名：OS_TaskAddEx
  * 功  能：添加单次任务
  * 参  数：TaskHandlerEx_tpf pfHandler - 处理函数
  *         U32 u32Parameter            - 参数
  * 返回值：结果
  * 说  明：拓展接口
*******************************************************************************/
Bool OS_TaskAddEx(TaskHandlerEx_tpf pfHandler, U32 u32Parameter)
{
    Bool bResult;
    Task_ts sTask;

    // 设置任务参数
    sTask.pfHandler    = pfHandler;
    sTask.u32Parameter = u32Parameter;
    sTask.eRunType     = eTASK_RUN_ONCE;

    // 添加任务到任务队列
    Hardware_EnterCritical();    // 进入临界区
    bResult = Queue_Add(&sTaskQueue, &sTask);
    Hardware_ExitCritical();     // 退出临界区

    // 返回结果
    return bResult;
}

/*******************************************************************************
  * 函数名：OS_TaskCreateEx
  * 功  能：创建永久任务
  * 参  数：TaskHandlerEx_tpf pfHandler - 处理函数
  *         U32 u32Parameter            - 参数
  * 返回值：结果
  * 说  明：拓展接口
*******************************************************************************/
Bool OS_TaskCreateEx(TaskHandlerEx_tpf pfHandler, U32 u32Parameter)
{
    Bool bResult;
    Task_ts sTask;

    // 设置任务参数
    sTask.pfHandler    = pfHandler;
    sTask.u32Parameter = u32Parameter;
    sTask.eRunType     = eTASK_RUN_ALWAYS;

    // 添加任务到任务队列
    Hardware_EnterCritical();    // 进入临界区
    bResult = Queue_Add(&sTaskQueue, &sTask);
    Hardware_ExitCritical();     // 退出临界区

    // 返回结果
    return bResult;
}

/*******************************************************************************
  * 函数名：OS_TaskTake
  * 功  能：取出任务
  * 参  数：Task_ts *psTask - 任务
  * 返回值：结果
  * 说  明：无
*******************************************************************************/
Bool OS_TaskTake(Task_ts *psTask)
{
    Bool bResult = FALSE;

    // 如果任务队列中有任务
    if(sTaskQueue.u16ElementsNum > 0)
    {
        // 从任务队列取出任务
        Hardware_EnterCritical();    // 进入临界区
        bResult = Queue_Remove(&sTaskQueue, psTask);
        Hardware_ExitCritical();     // 退出临界区
    }

    // 返回结果
    return bResult;
}

/*******************************************************************************
  * 函数名：OS_TaskGetNumber
  * 功  能：获取任务个数
  * 参  数：无
  * 返回值：任务个数
  * 说  明：无
*******************************************************************************/
U16 OS_TaskGetNumber(void)
{
    // 返回任务个数
    return sTaskQueue.u16ElementsNum;
}

/***************************** 文件结束 ***************************************/
