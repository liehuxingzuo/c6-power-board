/*******************************************************************************
  * 文件：OS_Timer.h
  * 作者：zyz
  * 版本：v2.0.1
  * 日期：2020-03-24
  * 说明：定时器
*******************************************************************************/
#ifndef __OS_TIMER_H
#define __OS_TIMER_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "Linklist.h"
#include "OS_Task.h"

/* 宏定义 *********************************************************************/
#define u16TIMER_EXPIRED_TIMES_MAX_VALUE      ((U16) 0xFFFF)    // 到期次数最大值
#define u8TIMER_START_DELAY_INCREASE_VALUE    ((U8)       9)    // 定时器启动增加值
#define u8TIMER_START_DELAY_MAX_VALUE         ((U8)     100)    // 定时器启动延时最大值

/* 类型定义 *******************************************************************/
// 定时器
typedef struct Timer_s
{
    LinklistNode_ts sListNode;    // 链表节点
    U16 u16Period;                // 周期
    U16 u16ExpiredTimes;          // 到期次数
    Task_ts psCallback;           // 回调任务
} Timer_ts;

// 定时器句柄
typedef Timer_ts *const TimerHandle_tps;

// 定时器控制
typedef struct TimerControl_s
{
    U16 u16TimerTick;                    // 定时器节拍
    Linklist_ts sTimerList1;             // 定时器链表1
    Linklist_ts sTimerList2;             // 定时器链表2
    Linklist_ts sTimerList3;             // 定时器链表3
    Linklist_ts *psCurrentTimerList;     // 当前定时器链表
    Linklist_ts *psOverflowTimerList;    // 溢出定时器链表
    Linklist_ts *psStagingTimerList;     // 暂存定时器链表
} TimerControl_ts;

/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
// 基本接口
void OS_TimerInit(void);                                    // 定时器初始化
void OS_TimerTask(void);                                    // 定时器任务
void OS_TimerStart(TimerHandle_tps psTimer,
                   U16 u16Period,
                   TaskHandler_tpf pfCallback);             // 开启定时器
void OS_TimerStop(TimerHandle_tps psTimer);                 // 停止定时器
void OS_TimerRestart(TimerHandle_tps psTimer,
                       U16 u16Period,
                       TaskHandler_tpf pfCallback);         // 重启定时器
void OS_TimerReset(TimerHandle_tps psTimer);                // 重置定时器
Bool OS_TimerIsActive(TimerHandle_tps psTimer);             // 定时器是否开启
void OS_TimerClearExpiredTimes(TimerHandle_tps psTimer);    // 清零到期次数
U16 OS_TimerGetExpiredTimes(TimerHandle_tps psTimer);       // 获取到期次数
U16 OS_TimerGetNumber(void);                                // 获取定时器个数
// 拓展接口
void OS_TimerStartEx(TimerHandle_tps psTimer,
                     Bool bDelay,
                     U16 u16Period,
                     TaskHandlerEx_tpf pfCallback,
                     U32 u32Parameter);                     // 开启定时器
void OS_TimerRestartEx(TimerHandle_tps psTimer,
                       U16 u16Period,
                       TaskHandlerEx_tpf pfCallback,
                       U32 u32Parameter);                   // 重启定时器

#endif /* __OS_TIMER_H */

/***************************** 文件结束 ***************************************/
