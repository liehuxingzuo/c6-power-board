/*******************************************************************************
  * 文件：Linklist.h
  * 作者：zyz
  * 版本：v2.0.1
  * 日期：2020-03-24
  * 说明：链表
*******************************************************************************/
#ifndef __LINKLIST_H
#define __LINKLIST_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"

/* 宏定义 *********************************************************************/
#define u16LINKLIST_MIN_NODE_VALUE    ((U16) 0x0000)    // 最小节点值
#define u16LINKLIST_MAX_NODE_VALUE    ((U16) 0xFFFF)    // 最大节点值


/* 类型定义 *******************************************************************/
struct Linklist_s;
struct LinklistNode_s;
// 节点
typedef struct LinklistNode_s
{
    U16 u16NodeValue;                     // 节点值
    struct LinklistNode_s *psNext;        // 后一节点
    struct LinklistNode_s *psPrevious;    // 前一节点
    struct Linklist_s *psContainer;       // 所在链表
    void *pvData;                         // 数据指针
} LinklistNode_ts;

// 链表
typedef struct Linklist_s
{
    LinklistNode_ts sListHead;    // 链表头
    LinklistNode_ts sListTail;    // 链表尾
    U16 u16NodesNum;              // 节点个数（不包括头和尾）
} Linklist_ts;

// 句柄
typedef Linklist_ts *const LinklistHandle_tps;            // 链表句柄
typedef LinklistNode_ts *const LinklistNodeHandle_tps;    // 节点句柄

/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
// 链表初始化
void Linklist_Init(LinklistHandle_tps psList);
// 插入新节点到链表头
void Linklist_InsertToHead(LinklistHandle_tps psList, LinklistNodeHandle_tps psNewNode);
// 插入新节点到链表尾
void Linklist_InsertToTail(LinklistHandle_tps psList, LinklistNodeHandle_tps psNewNode);
// 按照节点值插入新节点
void Linklist_InsertByValue(LinklistHandle_tps psList, LinklistNodeHandle_tps psNewNode);
// 移除节点
void Linklist_Remove(LinklistNodeHandle_tps psNodeToRemove);
// 获取节点个数
U16 Linklist_GetNodesNum(LinklistHandle_tps psList);

#endif /* __LINKLIST_H */

/***************************** 文件结束 ***************************************/
