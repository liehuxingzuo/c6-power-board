/*******************************************************************************
  * 文件：Queue.h
  * 作者：zyz
  * 版本：v2.0.1
  * 日期：2020-03-24
  * 说明：队列
*******************************************************************************/
#ifndef __QUEUE_H
#define __QUEUE_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
// 队列
typedef struct Queue_s
{
    void *pvDataBuffer;    // 数据缓冲区
    U16 u16QueueSize;      // 队列大小
    U16 u16ElementSize;    // 元素大小
    U16 u16HeadIndex;      // 头索引
    U16 u16TailIndex;      // 尾索引
    U16 u16ElementsNum;    // 元素个数
} Queue_ts;

// 句柄
typedef Queue_ts *const QueueHandle_tps;

/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
void Queue_Init(QueueHandle_tps psQueue, void *pvDataBuffer,
                U16 u16QueueSize, U16 u16ElementSize);          // 初始化队列
Bool Queue_Add(QueueHandle_tps psQueue, void *pvElement);       // 向队列添加元素
Bool Queue_Remove(QueueHandle_tps psQueue, void *pvElement);    // 从队列取出元素并移除
Bool Queue_Peek(QueueHandle_tps psQueue, void *pvElement);      // 从队列取出元素不移除
U16 Queue_GetElementsNum(QueueHandle_tps psQueue);              // 获取元素个数

#endif /* __QUEUE_H */

/***************************** 文件结束 ***************************************/
