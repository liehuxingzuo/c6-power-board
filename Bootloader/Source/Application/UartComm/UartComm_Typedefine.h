/*******************************************************************************
  * 文件：UartComm_Typedefine.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：数据类型
*******************************************************************************/
#ifndef __UARTCOMM_TYPEDEFINE_H
#define __UARTCOMM_TYPEDEFINE_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"
#include "UartComm_Constant.h"
#include "Queue.h"
#include "OS_Timer.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* <------------------------------- 驱动数据类型 ---------------------------> */
// 一般消息数据
typedef struct
{
    U8 au8Data[u8UARTCOMM_MAX_DATA_CRC_LEN];
} UartCommComMsgData_ts;

// 子命令消息数据
typedef struct
{
    U8 u8SubCmd;    // 子命令
    U8 au8Data[u8UARTCOMM_MAX_DATA_CRC_LEN - 1];
} UartCommSubCmdMsgData_ts;

// DBA消息数据
typedef struct
{
    U8 u8DataBlockNum;    // 数据块个数
    U8 au8Data[u8UARTCOMM_MAX_DATA_CRC_LEN - 1];
} UartCommDbaMsgData_ts;

// 协议包数据
typedef union
{
    UartCommComMsgData_ts sComMsgData;          // 一般消息数据
    UartCommSubCmdMsgData_ts sSubCmdMsgData;    // 子命令消息数据
    UartCommDbaMsgData_ts sDbaMsgData;          // DBA消息数据
} UartCommPacketData_tu;

// 协议包
typedef struct
{
    U8 u8SeqNum;     // 序列号
    U8 u8SrcAddr;    // 源地址
    U8 u8DstAddr;    // 目的地址
    U8 u8Command;    // 命令
    U8 u8DataLen;    // 数据长度
    UartCommPacketData_tu uData;    // 数据
} UartCommPacket_ts;

// 发送状态
typedef enum
{
	eSTATE_SEND_IDLE,    // 发送空闲
	eSTATE_SENT_STX,     // 已发送帧头
	eSTATE_SENT_BODY,    // 已发送帧体
	eSTATE_SENT_ETX      // 已发送帧尾
} UartCommSendState_te;

// 接收状态
typedef enum
{
	eSTATE_WAITING_STX,       // 等待帧头
	eSTATE_RECEIVING_BODY,    // 接收帧体
	eSTATE_WAITING_ETX        // 等待帧尾
} UartCommRecvState_te;

// 发送控制
typedef struct
{
    UartCommSendState_te eState;    // 发送状态
    UartCommPacket_ts sPacket;      // 数据包
    U8* pu8SendByte;                // 发送字节
    U8 u8SendByteCount;             // 发送字节计数
    Bool bEscCharActive;            // 转义字符激活标志
    U8 u8RetryTimes;                // 重试次数
    Timer_ts sSendTimer;            // 发送定时器
} UartCommSendControl_ts;

// 接收控制
typedef struct
{
    UartCommRecvState_te eState;    // 接收状态
    UartCommPacket_ts sPacket;      // 数据包
    U8* pu8RecvPos;                 // 接收位置
    U8 u8RecvByteCount;             // 接收字节计数
    Bool bEscCharActive;            // 转义字符激活标志
} UartCommRecvControl_ts;

// 串口通信控制
typedef struct
{
    void (*pfSendByte)(U8 u8Byte);          // 发送字节函数
    U8 (*pfRecvByte)(void);                 // 接收字节函数
    U8 u8LocalSeqNum;                       // 本地序列号
    UartCommSendControl_ts sSendControl;    // 发送控制
    UartCommRecvControl_ts sRecvControl;    // 接收控制
    Queue_ts sSendQueue;                    // 发送队列
    Queue_ts sRecvQueue;                    // 接收队列
    UartCommPacket_ts asSendBuffer[u8UARTCOMM_SEND_QUEUE_SIZE];    // 发送缓冲区
    UartCommPacket_ts asRecvBuffer[u8UARTCOMM_RECV_QUEUE_SIZE];    // 接收缓冲区
} UartCommControl_ts;

// 端口定义
typedef enum
{
    ePORT_DISPLAY_BOARD,    // 显示板
    ePORT_SUM,              // 端口总数
} UartCommPort_te;

// 串口通信
typedef struct
{
    UartCommPacket_ts sSendPacket;    // 待发送数据包
    UartCommPacket_ts sRecvPacket;    // 待处理数据包
    UartCommControl_ts asCommControl[ePORT_SUM];    // 通信控制
} UartComm_ts;

// 消息处理
typedef struct
{
    U8 u8Command;                                                             // 命令
    void (*pfHandler)(UartCommPort_te ePort, UartCommPacket_ts *psPacket);    // 处理函数
} UartCommMsgHandler_ts;

// 路由信息
typedef struct
{
    U8 u8Address;             // 地址
    UartCommPort_te ePort;    // 端口
} UartCommRoutingInfo_ts;

/* <------------------------------- DBA数据类型 ----------------------------> */
// 数据块
typedef struct
{
    U8 u8DataAddrMsb;    // 数据地址MSB
    U8 u8DataAddrLsb;    // 数据地址LSB
    U8 u8DataLen;        // 数据长度
    U8 au8Data[u8UARTCOMM_MAX_DATA_LEN - 4];
} UartCommDataBlock_ts;

// 订阅DBA参数
typedef struct
{
    U8 u8DeviceAddress;    // 设备地址
    U8 u8SubscribeTime;    // 订阅时间
    U8 u8DataBlockNum;     // 数据块个数
    U8 au8DataBlockParm[u8UARTCOMM_SUBSCRIBE_DB_PARM_LEN];    // 数据块参数
} UartCommSubscribeDbaParm_ts;

// 订阅DBA控制
typedef struct
{
    Timer_ts sSubscribeTimer;                      // 订阅定时器
    UartCommSubscribeDbaParm_ts sSubscribeParm;    // 订阅参数
} UartCommSubscribeDbaControl_ts;

// 数据读写
typedef enum
{
    eDATA_READ,         // 读取
    eDATA_WRITE,        // 写入
    eDATA_BITS_WRITE    // 位写入
} UartCommDataReadWrite_te;

// 数据块信息
typedef struct
{
    U16 u16DataAddr;    // 数据地址
    U8 *pu8Data;        // 数据指针
    U8 u8DataLen;       // 数据长度
} UartCommDataBlockInfo_ts;

// DBA信息
typedef struct
{
    UartCommDataBlockInfo_ts *psDataBlockInfo;
    U8 u8DataBlockInfoNum;
} UartCommDbaInfo_ts;

/* <------------------------------- OTA数据类型 ----------------------------> */

/* 变量定义 *******************************************************************/
/* 函数声明 *******************************************************************/


#endif    /*** #ifndef __UARTCOMM_TYPEDEFINE_H ***/

/***************************** 文件结束 ***************************************/


