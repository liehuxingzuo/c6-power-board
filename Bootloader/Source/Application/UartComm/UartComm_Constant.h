 /*******************************************************************************
  * 文件：UartComm_Constant.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：常量
*******************************************************************************/
#ifndef __UARTCOMM_CONSTANT_H
#define __UARTCOMM_CONSTANT_H

/* 头文件 *********************************************************************/
/* 宏定义 *********************************************************************/
// 字符定义
#define u8UARTCOMM_PKT_ESC    ((U8) 0xE0)    // ESC
#define u8UARTCOMM_PKT_ACK    ((U8) 0xE1)    // ACK
#define u8UARTCOMM_PKT_STX    ((U8) 0xE2)    // STX
#define u8UARTCOMM_PKT_ETX    ((U8) 0xE3)    // ETX

// 长度定义
#define u8UARTCOMM_MAX_PKT_LEN                        ((U8) 160)    // 最大包长度
#define u8UARTCOMM_SEQ_SRC_DST_CMD_LEN_LEN            ((U8)   5)    // SEQ+SRC+DST+CMD+LEN长度
#define u8UARTCOMM_SEQ_SRC_DST_CMD_LEN_CRC_LEN        ((U8)   7)    // SEQ+SRC+DST+CMD+LEN+CRC长度
#define u8UARTCOMM_MAX_BODY_LEN     (u8UARTCOMM_MAX_PKT_LEN - 2)    // 最大帧体长度
#define u8UARTCOMM_MAX_DATA_LEN     (u8UARTCOMM_MAX_PKT_LEN - 9)    // 最大数据长度
#define u8UARTCOMM_MAX_DATA_CRC_LEN (u8UARTCOMM_MAX_PKT_LEN - 7)    // 最大数据和校验长度

// 序列号
#define u8UARTCOMM_MIN_SEQ_NUM    ((U8)   1)    // 最小序列号
#define u8UARTCOMM_MAX_SEQ_NUM    ((U8) 200)    // 最大序列号

// 队列大小
#define u8UARTCOMM_SEND_QUEUE_SIZE    ((U8) 2)    // 发送队列大小
#define u8UARTCOMM_RECV_QUEUE_SIZE    ((U8) 2)    // 接收队列大小

// 订阅数据块参数长度
#define u8UARTCOMM_SUBSCRIBE_DB_PARM_LEN    ((U8) 30)

// 包CRC校验初始值
#define u16UARTCOMM_PKT_CRC_SEED    ((U16) 0x1021)

// 发送时间间隔（毫秒）
#define u16UARTCOMM_SEND_INTERVAL_MSEC     ((U16) 20)

// 设备地址
#define u8UARTCOMM_DISPLAY_BOARD_ADDR    ((U8) 0x40)    // 显示板地址
#define u8UARTCOMM_POWER_BOARD_ADDR      ((U8) 0x41)    // 电源板地址
// 测试工具地址，广播地址
#define u8UARTCOMM_TEST_TOOL_ADDR    ((U8) 0xE4)    // 测试工具地址
#define u8UARTCOMM_BROADCAST_ADDR    ((U8) 0xFF)    // 广播地址
// 本地地址
#define u8UARTCOMM_LOCAL_ADDR    u8UARTCOMM_POWER_BOARD_ADDR

/* 类型定义 *******************************************************************/
/* 变量定义 *******************************************************************/
/* 函数声明 *******************************************************************/


#endif    /*** #ifndef __UARTCOMM_CONSTANT_H ***/

/***************************** 文件结束 ***************************************/


