/*******************************************************************************
  * 文件：Debug.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-25
  * 说明：Debug打印接口
*******************************************************************************/
#ifndef _DEBUG_H
#define _DEBUG_H

/* 头文件 *********************************************************************/
#include "main.h"
#include "printk.h"
/* 宏定义 *********************************************************************/
// 宏定义DEBUG
#ifdef CONFIG_DEBUG
#define DEBUG(...)              printk(__VA_ARGS__)
#define DEBUG_ARR(array, len)   {									\
                                    int i;							\
                                    for(i=0;i<len;i++)				\
                                    {								\
                                        DEBUG("%02X ",array[i]);	\
                                    }								\
                                    DEBUG("\r\n");					\
                                }
#else
#define DEBUG(...)
#define DEBUG_ARR(arry,len)
#endif /* CONFIG_DEBUG*/

/* 类型定义 *******************************************************************/
/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/

#endif
