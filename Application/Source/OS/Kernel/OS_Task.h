/*******************************************************************************
  * 文件：OS_Task.h
  * 作者：zyz
  * 版本：v2.0.1
  * 日期：2020-03-24
  * 说明：任务
*******************************************************************************/
#ifndef __OS_TASK_H
#define __OS_TASK_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"

/* 宏定义 *********************************************************************/
// 任务队列大小
#define u8TASK_QUEUE_SIZE    ((U8) 32)

/* 类型定义 *******************************************************************/
// 任务处理函数
typedef void (*TaskHandler_tpf)(void);
typedef void (*TaskHandlerEx_tpf)(U32);

// 任务运行类型
typedef enum
{
    eTASK_RUN_ONCE   = 0,    // 运行一次
    eTASK_RUN_ALWAYS = 1     // 一直运行
} TaskRunType_te;

// 任务
typedef struct
{
    TaskHandlerEx_tpf pfHandler;    // 任务处理函数
    U32 u32Parameter;               // 处理函数参数
    TaskRunType_te eRunType;        // 任务运行类型
} Task_ts;

/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
// 基本接口
void OS_TaskInit(void);                           // 初始化
Bool OS_TaskAdd(TaskHandler_tpf pfHandler);       // 添加单次任务
Bool OS_TaskCreate(TaskHandler_tpf pfHandler);    // 创建永久任务
Bool OS_TaskTake(Task_ts *psTask);                // 取出任务
U16 OS_TaskGetNumber(void);                       // 获取任务个数
// 拓展接口
Bool OS_TaskAddEx(TaskHandlerEx_tpf pfHandler, U32 u32Parameter);       // 添加单次任务
Bool OS_TaskCreateEx(TaskHandlerEx_tpf pfHandler, U32 u32Parameter);    // 创建永久任务

#endif /* __OS_TASK_H */

/***************************** 文件结束 ***************************************/
