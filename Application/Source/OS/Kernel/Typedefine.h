/*******************************************************************************
  * 文件：Typedefine.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：类型定义
*******************************************************************************/
#ifndef __TYPEDEFINE_H
#define __TYPEDEFINE_H

/* 头文件 *********************************************************************/
#include <string.h>

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
// 数据类型
typedef signed char       S8;
typedef unsigned char     U8;
typedef signed short      S16;
typedef unsigned short    U16;
typedef signed long       S32;
typedef unsigned long     U32;
typedef unsigned char     Bool;
typedef float             F32;
typedef double            F64;

// 小端
typedef union
{
    U16 u16Word;
    struct
    {
        U8 u8Lsb;
        U8 u8Msb;
    } sByte;
} LittleEndianU16_tu;

typedef union
{
    U32 u32Word;
    struct
    {
        U8 u8LowLsb;
        U8 u8LowMsb;
        U8 u8HighLsb;
        U8 u8HighMsb;
    } sByte;
} LittleEndianU32_tu;
// 大端
typedef union
{
    U16 u16Word;
    struct
    {
        U8 u8Msb;
        U8 u8Lsb;
    } sByte;
} BigEndianU16_tu;

typedef union
{
    U32 u32Word;
    struct
    {
        U8 u8HighMsb;
        U8 u8HighLsb;
        U8 u8LowMsb;
        U8 u8LowLsb;
    } sByte;
} BigEndianU32_tu;

// 位转换
typedef union
{
    U8 u8Byte;
    struct
    {
        U8 bit0 : 1;
        U8 bit1 : 1;
        U8 bit2 : 1;
        U8 bit3 : 1;
        U8 bit4 : 1;
        U8 bit5 : 1;
        U8 bit6 : 1;
        U8 bit7 : 1;
    } sBit;
} U8ToBit_tu;

typedef union
{
    U16 u16Word;
    struct
    {
        U16 bit0  : 1;
        U16 bit1  : 1;
        U16 bit2  : 1;
        U16 bit3  : 1;
        U16 bit4  : 1;
        U16 bit5  : 1;
        U16 bit6  : 1;
        U16 bit7  : 1;
        U16 bit8  : 1;
        U16 bit9  : 1;
        U16 bit10 : 1;
        U16 bit11 : 1;
        U16 bit12 : 1;
        U16 bit13 : 1;
        U16 bit14 : 1;
        U16 bit15 : 1;
    } sBit;
} U16ToBit_tu;

typedef union
{
    U32 u32Word;
    struct
    {
        U32 bit0  : 1;
        U32 bit1  : 1;
        U32 bit2  : 1;
        U32 bit3  : 1;
        U32 bit4  : 1;
        U32 bit5  : 1;
        U32 bit6  : 1;
        U32 bit7  : 1;
        U32 bit8  : 1;
        U32 bit9  : 1;
        U32 bit10 : 1;
        U32 bit11 : 1;
        U32 bit12 : 1;
        U32 bit13 : 1;
        U32 bit14 : 1;
        U32 bit15 : 1;
        U32 bit16 : 1;
        U32 bit17 : 1;
        U32 bit18 : 1;
        U32 bit19 : 1;
        U32 bit20 : 1;
        U32 bit21 : 1;
        U32 bit22 : 1;
        U32 bit23 : 1;
        U32 bit24 : 1;
        U32 bit25 : 1;
        U32 bit26 : 1;
        U32 bit27 : 1;
        U32 bit28 : 1;
        U32 bit29 : 1;
        U32 bit30 : 1;
        U32 bit31 : 1;
    } sBit;
} U32ToBit_tu;

/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/


#endif /* __TYPEDEFINE_H */

/***************************** 文件结束 ***************************************/
