/*******************************************************************************
  * 文件：OS_Buffer.h
  * 作者：zyz
  * 版本：v2.0.1
  * 日期：2020-03-24
  * 说明：缓冲区
*******************************************************************************/
#ifndef __OS_BUFFER_H
#define __OS_BUFFER_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
// 缓冲区
typedef struct Buffer_s
{
    U8 *pu8Buffer;        // 缓冲区
    U16 u16BufferSize;    // 缓冲区大小
    U16 u16WriteIndex;    // 写索引
    U16 u16ReadIndex;     // 读索引
    U16 u16DataNum;       // 数据个数
} Buffer_ts;

// 句柄
typedef Buffer_ts *const BufferHandle_tps;

/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
// 初始化
void OS_BufferInit(BufferHandle_tps psBuffer, U8 *pu8Buffer, U16 u16BufferSize);
// 向缓冲区写入数据
U16 OS_BufferWrite(BufferHandle_tps psBuffer, U8 *pu8Data, U16 u16DataLen);
// 从缓冲区移除数据
U16 OS_BufferRemove(BufferHandle_tps psBuffer, U8 *pu8Data, U16 u16DataLen);
// 从缓冲区查看数据
U16 OS_BufferPeek(BufferHandle_tps psBuffer, U8 *pu8Data, U16 u16DataLen);
// 获取缓冲区数据个数
U16 OS_BufferGetDataNum(BufferHandle_tps psBuffer);

#endif /* __OS_BUFFER_H */

/***************************** 文件结束 ***************************************/
