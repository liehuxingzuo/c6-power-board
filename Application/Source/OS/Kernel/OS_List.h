/*******************************************************************************
  * 文件：OS_List.h
  * 作者：zyz
  * 版本：v2.0.1
  * 日期：2020-03-24
  * 说明：链表
*******************************************************************************/
#ifndef __OS_LIST_H
#define __OS_LIST_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"

/* 宏定义 *********************************************************************/
#define u16LIST_MIN_NODE_VALUE    ((U16) 0x0000)    // 最小节点值
#define u16LIST_MAX_NODE_VALUE    ((U16) 0xFFFF)    // 最大节点值


/* 类型定义 *******************************************************************/
struct List_s;
struct ListNode_s;
// 节点
typedef struct ListNode_s
{
    U16 u16NodeValue;                 // 节点值
    struct ListNode_s *psNext;        // 后一节点
    struct ListNode_s *psPrevious;    // 前一节点
    struct List_s *psContainer;       // 所在链表
    void *pvData;                     // 数据指针
} ListNode_ts;

// 链表
typedef struct List_s
{
    ListNode_ts sListHead;    // 链表头
    ListNode_ts sListTail;    // 链表尾
    U16 u16NodesNum;          // 节点个数（不包括头和尾）
} List_ts;

// 句柄
typedef List_ts *const ListHandle_tps;            // 链表句柄
typedef ListNode_ts *const ListNodeHandle_tps;    // 节点句柄

/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
// 链表初始化
void OS_ListInit(ListHandle_tps psList);
// 插入新节点到链表头
void OS_ListInsertToHead(ListHandle_tps psList, ListNodeHandle_tps psNewNode);
// 插入新节点到链表尾
void OS_ListInsertToTail(ListHandle_tps psList, ListNodeHandle_tps psNewNode);
// 按照节点值插入新节点
void OS_ListInsertByValue(ListHandle_tps psList, ListNodeHandle_tps psNewNode);
// 移除节点
void OS_ListRemove(ListNodeHandle_tps psNodeToRemove);
// 获取节点个数
U16 OS_ListGetNodesNum(ListHandle_tps psList);

#endif /* __OS_LIST_H */

/***************************** 文件结束 ***************************************/
