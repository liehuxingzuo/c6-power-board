/*******************************************************************************
  * 文件：Constant.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：常量定义
*******************************************************************************/
#ifndef __CONSTANT_H
#define __CONSTANT_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"

/* 宏定义 *********************************************************************/

// TRUE和FALSE
#define TRUE     (1)
#define FALSE    (0)
// PASS和FAIL
#define PASS     (0)
#define FAIL     (1)
// OFF和ON
#define OFF      (0)
#define ON       (1)
// HIGH和LOW
#define LOW      (0)
#define HIGH     (1)

// 位掩码
#define u8MASK_SET_BIT7    (U8)0x80
#define u8MASK_SET_BIT6    (U8)0x40
#define u8MASK_SET_BIT5    (U8)0x20
#define u8MASK_SET_BIT4    (U8)0x10
#define u8MASK_SET_BIT3    (U8)0x08
#define u8MASK_SET_BIT2    (U8)0x04
#define u8MASK_SET_BIT1    (U8)0x02
#define u8MASK_SET_BIT0    (U8)0x01
#define u8MASK_CLR_BIT7    (U8)0x7F
#define u8MASK_CLR_BIT6    (U8)0xBF
#define u8MASK_CLR_BIT5    (U8)0xDF
#define u8MASK_CLR_BIT4    (U8)0xEF
#define u8MASK_CLR_BIT3    (U8)0xF7
#define u8MASK_CLR_BIT2    (U8)0xFB
#define u8MASK_CLR_BIT1    (U8)0xFD
#define u8MASK_CLR_BIT0    (U8)0xFE

// 数据边界值
#define s8MAX_VALUE     ((S8) 0x7F)                    // S8最大值
#define s8MIN_VALUE     ((U8) (-s8MAX_VALUE - 1))      // S8最小值
#define u8MAX_VALUE     ((U8) 0xFF)                    // U8最大值
#define u8MIN_VALUE     ((U8) 0)                       // U8最小值
#define s16MAX_VALUE    ((S16) 0x7FFF)                 // S16最大值
#define s16MIN_VALUE    ((S16) (-s16MAX_VALUE - 1))    // S16最小值
#define u16MAX_VALUE    ((U16) 0xFFFF)                 // U16最大值
#define u16MIN_VALUE    ((U16) 0)                      // U16最小值
#define s32MAX_VALUE    ((S32) 0x7FFFFFFF)             // S32最大值
#define s32MIN_VALUE    ((S32) (-s32MAX_VALUE - 1))    // S32最小值
#define u32MAX_VALUE    ((U32) 0xFFFFFFFF)             // U32最大值
#define u32MIN_VALUE    ((U32) 0)                      // U32最小值

// 数据类型位数
#define u8BITS_IN_U8     ((U8) 8)
#define u8BITS_IN_U16    ((U8) 16)
#define u8BITS_IN_U32    ((U8) 32)
#define u8BITS_IN_U64    ((U8) 64)

// 时间相关
#define u16HOUR_TO_MIN    ((U16)  3600)    // 小时到分钟
#define u16HOUR_TO_SEC    ((U16)  3600)    // 小时到秒
#define u16MIN_TO_SEC     ((U16)    60)    // 分钟到秒
#define u16MIN_TO_MSEC    ((U32) 60000)    // 分钟到毫秒
#define u16SEC_TO_MSEC    ((U16)  1000)    // 秒到毫秒

/* 类型定义 *******************************************************************/
/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/

#endif /* __CONSTANT_H */

/***************************** 文件结束 ***************************************/
