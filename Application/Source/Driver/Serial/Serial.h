/*******************************************************************************
  * 文件：Serial.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-05
  * 说明：串口驱动
*******************************************************************************/
#ifndef _SERIAL_H
#define _SERIAL_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"
#include "SerialFIFO.h"
/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
typedef enum
{
    eSERIAL_IDEL = 0,
    eSERIAL_BUSY = 1,
}SerialTXState_te;

void SerialRecvData(SERIAL_FIFO_CHNS eCHNn,U8 au8Data[],U8 u8Len); // 串口接收数据
void SerialSendData(SERIAL_FIFO_CHNS eCHNn,U8 au8Data[],U8 u8Len); // 串口发送数据（异步）
void SerialSendEndIRQHook(SERIAL_FIFO_CHNS eCHNn);// 串口发送完成中断回调函数
void Serial_Init(void);// 串口驱动初始化

#endif

