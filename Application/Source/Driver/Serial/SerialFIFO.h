/*******************************************************************************
  * 文件：SerialFIFO.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-05
  * 说明：串口发送接收缓冲区
*******************************************************************************/
#ifndef _UARTFIFO_H
#define _UARTFIFO_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"
#include "OS_Buffer.h"

/* 宏定义 *********************************************************************/
#define U8_SERIAL_TX_BUFFER_SZIE        ((U8) 128)
#define U8_SERIAL_RX_BUFFER_SZIE        ((U8) 128)

/* 类型定义 *******************************************************************/
typedef enum
{
    SERIAL_BOARD_OXYGEN_CHN = 0,	// 氧气传感器通信
    SERIAL_CHN_SUM,	            // 通道总数
}SERIAL_FIFO_CHNS;// 串口通道

typedef struct
{
    U8 au8TXBuffer[U8_SERIAL_TX_BUFFER_SZIE];
    U8 au8RXBuffer[U8_SERIAL_RX_BUFFER_SZIE];
    Buffer_ts sTxBuffer;
    Buffer_ts sRxBuffer;
}SerialFIFO_ts;// 串口FIFO管理

void SerialFIFOInit(void);// 串口缓冲区初始化
U16  SerialTxFIFORead(SERIAL_FIFO_CHNS chn,U8 buffer[],U8 len);// 串口发送缓冲区读数据
U16  SerialTxFIFOWrite(SERIAL_FIFO_CHNS chn,U8 buffer[],U8 len);// 串口发送缓冲区写数据
void SerialTxFIFOClear(SERIAL_FIFO_CHNS chn, U8 len);// 串口发送缓冲区清空数据

U16  SerialRxFIFOPeek(SERIAL_FIFO_CHNS chn,U8 buffer[],U8 len);// 串口接收缓冲区读数据（不清空）
U16  SerialRxFIFORead(SERIAL_FIFO_CHNS chn,U8 buffer[],U8 len);// 串口接收缓冲区读数据（清空）
U16  SerialRxFIFOWrite(SERIAL_FIFO_CHNS chn,U8 buffer[],U8 len);// 串口接收缓冲区写数据
void SerialRxFIFOClear(SERIAL_FIFO_CHNS chn, U8 len);// 串口接收缓冲区清空数据

#endif
