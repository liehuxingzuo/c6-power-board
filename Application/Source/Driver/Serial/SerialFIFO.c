/*******************************************************************************
  * 文件：SerialFIFO.c
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-05
  * 说明：串口发送接收缓冲区
*******************************************************************************/
#include "SerialFIFO.h"
#include "ComFun.h"

static SerialFIFO_ts asSerialFIFO[SERIAL_CHN_SUM];
/*******************************************************************************
  * 函数名：SerialFIFOInit
  * 功  能：串口缓冲区初始化
  * 参  数：无
  * 返回值：无
  * 说  明：初始化串口接收、发送缓冲区
*******************************************************************************/
void SerialFIFOInit(void)
{
    U8 u8Index;

    // 初始化所有串口通道
    for(u8Index=0;u8Index < SERIAL_CHN_SUM;u8Index++)
    {
        // 初始化串口接收缓冲区
        OS_BufferInit(&(asSerialFIFO[u8Index].sRxBuffer),
                      asSerialFIFO[u8Index].au8RXBuffer,
                      U8_SERIAL_RX_BUFFER_SZIE);

        // 初始化串口发送缓冲区
        OS_BufferInit(&(asSerialFIFO[u8Index].sTxBuffer),
                      asSerialFIFO[u8Index].au8TXBuffer,
                      U8_SERIAL_TX_BUFFER_SZIE);
    }
}

/*******************************************************************************
  * 函数名：UartTxFIFORead
  * 功  能：串口发送缓冲区中读取数据，读取出的数据在缓冲区中被清空
  * 参  数： SERIAL_FIFO_CHNS chn       - 串口通道号
  *         U8 buffer[]         - 读取出的数据存放位置
  *         U8 len              - 计划读取出的数据长度
  * 返回值：1.若缓冲区中实际存放的数据长度大于等于len且读取成功，返回len
  *         2.若缓冲区中实际存放的数据长度小于len且读取成功，返回数据的实际长度
  *         3.读取失败，返回0
  * 说  明：无
*******************************************************************************/
U16 SerialTxFIFORead(SERIAL_FIFO_CHNS chn,U8 buffer[] ,U8 len)
{
    return OS_BufferRemove(&(asSerialFIFO[chn].sTxBuffer),buffer,len);
}

/*******************************************************************************
  * 函数名：UartTxFIFOWrite
  * 功  能：串口发送缓冲区中写入数据
  * 参  数： SERIAL_FIFO_CHNS chn       - 串口通道号
  *         U8 buffer[]         - 写入数据的存放位置
  *         U8 len              - 计划写入的数据长度
  * 返回值：1.若全部数据写入成功，返回写入的数据长度len
  *         2.若写入过程中缓冲区已满，则返回已写入部分的数据长度
  *         3.若写入失败，返回0
  * 说  明：无
*******************************************************************************/
U16 SerialTxFIFOWrite(SERIAL_FIFO_CHNS chn,U8 buffer[],U8 len)
{
    return OS_BufferWrite(&(asSerialFIFO[chn].sTxBuffer),buffer,len);
}

/*******************************************************************************
  * 函数名：UartRxFIFOWrite
  * 功  能：串口接收缓冲区中写入数据
  * 参  数： SERIAL_FIFO_CHNS chn       - 串口fifo编号
  *         U8 buffer[]         - 写入数据的存放位置
  *         U8 len              - 计划写入的数据长度
  * 返回值：1.若全部数据写入成功，返回写入的数据长度len
  *        2.若写入过程中缓冲区已满，则返回已写入部分的数据长度
  *        3.若写入失败，返回0
  * 说  明：无
*******************************************************************************/
U16 SerialRxFIFOWrite(SERIAL_FIFO_CHNS chn,U8 buffer[] ,U8 len)
{
    return OS_BufferWrite(&(asSerialFIFO[chn].sRxBuffer),buffer,len);
}

/*******************************************************************************
  * 函数名：UartRxFIFOPeek
  * 功  能：串口接收缓冲区中读取数据，读取出的数据在缓冲区中保留，不清空
  * 参  数： SERIAL_FIFO_CHNS chn       - 串口fifo编号
  *         U8 buffer[]         - 读取出的数据存放位置
  *         U8 len              - 计划读取出的数据长度
  * 返回值：1.若缓冲区中实际存放的数据长度大于等于len且读取成功，返回len
  *        2.若缓冲区中实际存放的数据长度小于len且读取成功，返回数据的实际长度
  *        3.读取失败，返回0
  * 说  明：调用此函数，从串口fifo中读取数据，读取的数据依旧保留在缓冲区，不被清空
*******************************************************************************/
U16  SerialRxFIFOPeek(SERIAL_FIFO_CHNS chn,U8 buffer[],U8 len)
{
    return OS_BufferPeek(&(asSerialFIFO[chn].sRxBuffer),buffer,len);
}

/*******************************************************************************
  * 函数名：UartRxFIFORead
  * 功  能：串口接收缓冲区中读取数据，清空缓冲区中已经读取出来的数据
  * 参  数： SERIAL_FIFO_CHNS chn       - 串口fifo编号
  *         U8 buffer[]         - 读取出的数据存放位置
  *         U8 len              - 计划读取出的数据长度
  * 返回值：1.若缓冲区中实际存放的数据长度大于等于len且读取成功，返回len
  *        2.若缓冲区中实际存放的数据长度小于len且读取成功，返回数据的实际长度
  *        3.读取失败，返回0
  * 说  明：调用此函数，从串口fifo中读取数据，读取的数据被清空
*******************************************************************************/
U16  SerialRxFIFORead(SERIAL_FIFO_CHNS chn,U8 buffer[],U8 len)
{
    return OS_BufferRemove(&(asSerialFIFO[chn].sRxBuffer),buffer,len);
}

/*******************************************************************************
  * 函数名：UartRxFIFOClear
  * 功  能：清空串口接收缓冲区
  * 参  数： SERIAL_FIFO_CHNS chn   - 串口fifo编号
  *         U8 len               - 要清空的数据长度
  * 返回值：无
  * 说  明：此函数用于清空串口接收缓冲区
*******************************************************************************/
void SerialRxFIFOClear(SERIAL_FIFO_CHNS chn, U8 len)
{
    U16 u16Index;
    U16 u16DataNum = OS_BufferGetDataNum(&(asSerialFIFO[chn].sRxBuffer));

    for(u16Index = 0;u16Index < MIN(u16DataNum,len);u16Index++)
    {
        U8 u8Temp;
        
        OS_BufferRemove(&(asSerialFIFO[chn].sRxBuffer),&u8Temp,sizeof(U8));
    }
}

/*******************************************************************************
  * 函数名：UartTxFIFOClear
  * 功  能：清空串口发送缓冲区
  * 参  数： SERIAL_FIFO_CHNS chn       - 串口fifo编号
  *         U8 len              - 要清空的数据长度
  * 返回值：无
  * 说  明：此函数用于清空串口发送缓冲区
*******************************************************************************/
void SerialTxFIFOClear(SERIAL_FIFO_CHNS chn, U8 len)
{
    U16 u16Index;
    U16 u16DataNum = OS_BufferGetDataNum(&(asSerialFIFO[chn].sTxBuffer));

    for(u16Index = 0;u16Index < MIN(u16DataNum,len);u16Index++)
    {
        U8 u8Temp;
        
        OS_BufferRemove(&(asSerialFIFO[chn].sTxBuffer),&u8Temp,sizeof(U8));
    }  
}

