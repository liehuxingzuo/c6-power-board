/*******************************************************************************
  * 文件：Serial.c
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-05
  * 说明：串口驱动
*******************************************************************************/
#include "Serial.h"
#include "Hardware_Uart.h"

static U8 au8SerialTxState[SERIAL_CHN_SUM];
/*******************************************************************************
  * 函数名：SerialRecvData
  * 功  能：串口接收数据
  * 参  数：SERIAL_FIFO_CHNS eCHNn：串口通道号
           U8 au8Data[]：待接收的数据
           U8 u8Len：待接收的数据长度
  * 返回值：无
  * 说  明：将串口接收的数据写入接收缓冲区，等待其他进程取出数据并解析。
  * 此过程为异步解析。
*******************************************************************************/
void SerialRecvData(SERIAL_FIFO_CHNS eCHNn,U8 au8Data[],U8 u8Len)
{    
    // 传入参数非法，提前退出
    if(eCHNn >= SERIAL_CHN_SUM || au8Data == NULL || u8Len == 0)
    {
        return;
    }

    // 将数据存入接收缓冲区
    SerialRxFIFOWrite(eCHNn,au8Data,u8Len);
}

/*******************************************************************************
  * 函数名：SerialSendData
  * 功  能：串口发送数据（异步）
  * 参  数：SERIAL_FIFO_CHNS eCHNn：串口通道号
           U8 au8Data[]：待发送的数据
           U8 u8Len：待发送的数据长度
  * 返回值：无
  * 说  明：将待发送的数据写入串口发送缓冲区，并开启串口发送空中断。在发送中断
  * 中将数据从发送缓冲区取出并发送，此过程为异步发送。
*******************************************************************************/
void SerialSendData(SERIAL_FIFO_CHNS eCHNn,U8 au8Data[],U8 u8Len)
{
    // 传入参数非法，提前退出
    if(eCHNn >= SERIAL_CHN_SUM || au8Data == NULL || u8Len == 0)
    {
        return;
    }
    
    // 将待发送的数据写入发送缓冲区
    SerialTxFIFOWrite(eCHNn,au8Data,u8Len);

    // 根据串口通道定义，开启发送中断，在中断中将数据发送
    switch (eCHNn)
    {
        case SERIAL_BOARD_OXYGEN_CHN:
        	// 若发送状态为空闲，则启动发送
			if(au8SerialTxState[eCHNn] == eSERIAL_IDEL)
			{
				U8 u8Data;
				SerialTxFIFORead(SERIAL_BOARD_OXYGEN_CHN,&u8Data,1);
				Hardware_Uart0SendByte(u8Data);
				au8SerialTxState[eCHNn] = eSERIAL_BUSY;
			}
        	break;
        default:
            break;
    }
}

/*******************************************************************************
  * 函数名：SerialSendEndIRQHook
  * 功  能：串口发送完成中断回调函数
  * 参  数：SERIAL_FIFO_CHNS eCHNn：串口通道
  * 返回值：无
  * 说  明：在发送完成中断中，继续读取发送缓冲区，将数据发送，直到数据全部发完。
*******************************************************************************/
void SerialSendEndIRQHook(SERIAL_FIFO_CHNS eCHNn)
{
    U8 u8Data;

    // 从串口发送缓冲区取出数据
    if(SerialTxFIFORead(eCHNn,&u8Data,1))
    {
        // 数据取出，将数据发送
        switch (eCHNn)
        {
            case SERIAL_BOARD_OXYGEN_CHN:
            	Hardware_Uart0SendByte(u8Data);
            	break;
            default:
                break;
        }
    }
    else
    {
        // 数据已发送完，更新发送状态为空闲
        au8SerialTxState[eCHNn] = eSERIAL_IDEL;
    }
}

/*******************************************************************************
  * 函数名：Serial_Init
  * 功  能：串口驱动初始化
  * 参  数：无
  * 返回值：无
  * 说  明：初始化串口缓冲区
*******************************************************************************/
void Serial_Init(void)
{
    // 初始化串口缓冲区
    SerialFIFOInit();    
}

