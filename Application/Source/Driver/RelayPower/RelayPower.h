/*******************************************************************************
  * 文件：RelayPower.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-04
  * 说明：继电器电源控制相关，继电器12V供电和零总继电器开启时需要输出1KHZ方波。
*******************************************************************************/
#ifndef _RELAY_POWER_H
#define _RELAY_POWER_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"
/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
Bool RelayGetZeroLine1State(void);      // 获取零总1控制状态
Bool RelayGetZeroLine2State(void);      // 获取零总2控制状态
void RelayZeroLine1Ctrl(Bool bState);   // 零总1控制
void RelayZeroLine2Ctrl(Bool bState);   // 零总2控制
void Relay12VolCtrl(Bool bState);       // 继电器12V供电控制控制

void RelayZeroLine1CtrlHook_500us(void);// 零总1控制500us回调
void RelayZeroLine2CtrlHook_500us(void);// 零总2控制500us回调
void Relay12VolCtrlHook_500us(void);    // 继电器12V供电控制500us回调

#endif

