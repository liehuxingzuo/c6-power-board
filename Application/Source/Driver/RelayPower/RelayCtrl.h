/*******************************************************************************
  * 文件：RelayPower.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-04
  * 说明：负载继电器控制，多个继电器不允许同时动作，须至少间隔220ms
*******************************************************************************/
#ifndef _RELAY_CTRL_H
#define _RELAY_CTRL_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"
#include "OS_Timer.h"
/* 宏定义 *********************************************************************/
#define U8_RELAY_NUMS               ((U8)  13)  // 负载继电器数量

/* 类型定义 *******************************************************************/
typedef struct
{
	Bool bRelayReserve		:	1;		// 预留继电器
	Bool bRelayBackFan1		:	1;		// 背部风扇+
	Bool bRelayBackFan2		:	1;		// 背部风扇-
	Bool bRelayMotor		:	1;		// 蜡马达
	Bool bRelayCoolFan1		:	1;		// 散热风扇A
	Bool bRelayCoolFan2		:	1;		// 散热风扇B
	Bool bRelaySteamPanOut	:	1;		// 蒸发盘外
	Bool bRelaySteamPanIn	:	1;		// 蒸发盘内

	Bool bRelayUpInHeatTube	:	1;		// 上内加热管
	Bool bRelayUpOutHeatTube:	1;		// 上外加热管
	Bool bRelayDownHeatTube	:	1;		// 下加热管
	Bool bRelayLamp			:	1;		// 炉灯
	Bool bRelayBackHeatTube	:	1;		// 背部加热管
	U8	 u3Reserve			:	3;		// 预留bit
}RelayBitMap_ts;// 负载继电器控制位图

typedef union
{
	RelayBitMap_ts sRelayStatus;
	U16			   u16RelayStatus;
}RelayBitMap_tu;// 负载继电器控制位图共用体

typedef struct
{
	RelayBitMap_tu	uRelayStatus;		// 继电器状态
	RelayBitMap_tu	uRelayStatusBak;	// 继电器状态备份，主要用于实现间隔210ms动作
	Timer_ts		sTimer;				// 软件定时器
}RelayCtrl_ts;// 继电器控制

/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
// 负载继电器控制API
void RelayCtrlReserve(Bool bState);
void RelayCtrlBackFan1(Bool bState);
void RelayCtrlBackFan2(Bool bState);
void RelayCtrlMotor(Bool bState);
void RelayCtrlCoolFan1(Bool bState);
void RelayCtrlCoolFan2(Bool bState);
void RelayCtrlSteamPanOut(Bool bState);
void RelayCtrlSteamPanIn(Bool bState);
void RelayCtrlUpInHeatTube(Bool bState);
void RelayCtrlUpOutHeatTube(Bool bState);
void RelayCtrlDownHeatTube(Bool bState);
void RelayCtrlLamp(Bool bState);
void RelayCtrlBackHeatTube(Bool bState);

// 获取负载继电器控制状态
Bool RelayGetReserveState(void);
Bool RelayGetBackFan1State(void);
Bool RelayGetBackFan2State(void);
Bool RelayGetMotorState(void);
Bool RelayGetCoolFan1State(void);
Bool RelayGetCoolFan2State(void);
Bool RelayGetSteamPanOutState(void);
Bool RelayGetSteamPanInState(void);
Bool RelayGetUpInHeatTubeState(void);
Bool RelayGetUpOutHeatTubeState(void);
Bool RelayGetDownHeatTubeState(void);
Bool RelayGetLampState(void);
Bool RelayGetBackHeatTubeState(void);

// 继电器控制220ms定时回调函数
void RelayCtrlHook_220MS(void);

// 继电器控制初始化
void RelayCtrl_Init(void);

#endif

