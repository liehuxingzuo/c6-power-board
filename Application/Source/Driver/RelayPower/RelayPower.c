/*******************************************************************************
  * 文件：RelayPower.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-04
  * 说明：继电器电源控制相关
*******************************************************************************/
#include "RelayPower.h"
#include "Hardware_IO.h"

static volatile Bool bRelayZeroLine1Enable = FALSE;
static volatile Bool bRelayZeroLine2Enable = FALSE;
static volatile Bool bRelay12VolEnable = FALSE;
/*******************************************************************************
  * 函数名：RelayZeroLine1CtrlHook_500us
  * 功  能：零总1继电器供电控制500us回调函数
  * 参  数：无
  * 返回值：无
  * 说  明：零总继电器打开时，需要输出1HZ方波
*******************************************************************************/
void RelayZeroLine1CtrlHook_500us(void)
{
    volatile static U16 u16State = 0;

    // 使能零总1，产生1Hz的方波
    if(bRelayZeroLine1Enable)
    {
        if(u16State)
        {
           Hardware_ZeroLine1Ctrl(ON); 
           u16State = 0;
        }
        else
        {
           Hardware_ZeroLine1Ctrl(OFF); 
           u16State = 1;
        }
    }
    // 关闭零总1，将引脚置低
    else
    {
        Hardware_ZeroLine1Ctrl(OFF);
    }
}

/*******************************************************************************
  * 函数名：RelayZeroLine2CtrlHook_500us
  * 功  能：零总2继电器供电控制500us回调函数
  * 参  数：无
  * 返回值：无
  * 说  明：零总继电器打开时，需要输出1HZ方波
*******************************************************************************/
void RelayZeroLine2CtrlHook_500us(void)
{
    volatile static U16 u16State = 0;

    // 使能零总2，产生1Hz的方波
    if(bRelayZeroLine2Enable)
    {
        if(u16State)
        {
           Hardware_ZeroLine2Ctrl(ON); 
           u16State = 0;
        }
        else
        {
           Hardware_ZeroLine2Ctrl(OFF); 
           u16State = 1;
        }
    }
    // 关闭零总2，将引脚置低
    else
    {
        Hardware_ZeroLine2Ctrl(OFF);
    }
}

/*******************************************************************************
  * 函数名：Relay12VolCtrlHook_500us
  * 功  能：继电器12V供电控制500us回调函数
  * 参  数：无
  * 返回值：无
  * 说  明：继电器12v供电打开时，需要输出1HZ方波
*******************************************************************************/
void Relay12VolCtrlHook_500us(void)
{
    volatile static U16 u16State = 0;

    // 使能12V，产生1Hz的方波
    if(bRelay12VolEnable)
    {
        if(u16State)
        {
           Hardware_12VolCtrl(ON); 
           u16State = 0;
        }
        else
        {
           Hardware_12VolCtrl(OFF); 
           u16State = 1;
        }
    }
    // 关闭12V，将引脚置低
    else
    {
        Hardware_12VolCtrl(OFF);
    }
}

/*******************************************************************************
  * 函数名：RelayZeroLine1Ctrl
  * 功  能：零总1继电器开关控制
  * 参  数：Bool bState：开关状态
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void RelayZeroLine1Ctrl(Bool bState)
{
    bRelayZeroLine1Enable = bState;
}

/*******************************************************************************
  * 函数名：RelayZeroLine2Ctrl
  * 功  能：零总2继电器开关控制
  * 参  数：Bool bState：开关状态
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void RelayZeroLine2Ctrl(Bool bState)
{
    bRelayZeroLine2Enable = bState;
}

/*******************************************************************************
  * 函数名：RelayGetZeroLine1State
  * 功  能：获取零总1状态
  * 参  数：无
  * 返回值：Bool bState：开关状态
  * 说  明：无
*******************************************************************************/
Bool RelayGetZeroLine1State(void)
{
	return bRelayZeroLine1Enable;
}

/*******************************************************************************
  * 函数名：RelayGetZeroLine2State
  * 功  能：获取零总2状态
  * 参  数：无
  * 返回值：Bool bState：开关状态
  * 说  明：无
*******************************************************************************/
Bool RelayGetZeroLine2State(void)
{
	return bRelayZeroLine2Enable;
}

/*******************************************************************************
  * 函数名：Relay12VolCtrl
  * 功  能：继电器12V供电控制
  * 参  数：Bool bState：开关状态
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void Relay12VolCtrl(Bool bState)
{
    bRelay12VolEnable = bState;
}

