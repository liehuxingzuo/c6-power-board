/*******************************************************************************
  * 文件：OxygenSensor.c
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-01-20
  * 说明：氧气传感器驱动
*******************************************************************************/
#ifndef _OXYGEN_SENSOR_H
#define _OXYGEN_SENSOR_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"
#include "Serial.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
#pragma pack(1)
typedef struct
{
	U8	u8FMT;
	U8	u8TRG;
	U8	u8SRC;
	U8	u8LEN;
	U8	u8DATA1;
	U8	u8DATA2;
	U8	u8DATA3;
	U8	u8CS;
}OxygenFrame_ts; // 氧传感器通信报文帧格式
#pragma pack()

Bool OxygenSensorIsWork(void);  // 检查氧气传感器是否在工作
void OxygenSensor_Stop(void);   // 氧气传感器暂停工作
void OxygenSensor_Start(void);  // 氧气传感器启动工作
void OxygenSensor_Init(void);   // 氧气传感器初始化

#endif

