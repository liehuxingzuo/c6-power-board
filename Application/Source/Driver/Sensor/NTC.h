/*******************************************************************************
  * 文件：NTC.c
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-01-20
  * 说明：NTC传感器驱动
*******************************************************************************/
#ifndef _NTC_H
#define _NTC_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
typedef struct
{
    U16 u16Temper;	// 温度
    U16 u16ADC;		// ADC值
}RTTable_ts;// RT表格式

U16 NTC_GetTemperFromNTCTable(U16 u16ADC);	    // NTC上拉分压电阻75K-RT表
U16 NTC_GetTemperFromProbeRTTable(U16 u16ADC);  // 肉感探针RT表
U16 NTC_GetTemperFromCavityNTCRTTable1(U16 u16ADC);// 腔体NTC查表1
U16 NTC_GetTemperFromCavityNTCRTTable2(U16 u16ADC);// 腔体NTC查表2

#endif

