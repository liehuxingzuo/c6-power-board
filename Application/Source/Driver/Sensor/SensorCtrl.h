/*******************************************************************************
  * 文件：SensorCtrl.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-20
  * 说明：传感器驱动
*******************************************************************************/
#ifndef _SENSOR_CTRL_H
#define _SENSOR_CTRL_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"
#include "OS_Timer.h"

/* 宏定义 *********************************************************************/
#define  U16_SENSOR_OPEN_ADC_MAX	((U16) 1022)	// 传感器断路故障时ADC采集阈值
#define  U16_SENSOR_SHORT_ADC_MIN	((U16) 0)		// 传感器短路故障时ADC采集阈值

/* 类型定义 *******************************************************************/
typedef enum
{
	eSENSOR_ERR_NO = 0, // 无故障
	eSENSOR_ERR_SHORT,	// 短路故障
	eSENSOR_ERR_OPEN,	// 短路故障
}SensorErr_te; // 传感器故障类型

typedef enum
{
    eCavitySensor_PT1000 = 0,   // PT1000传感器
    eCavitySensor_NTC,          // NTC传感器
}CavitySensorType_te;// 腔体传感器类型

typedef enum
{
    eProbeSensor_PT1000 = 0,   // PT1000传感器
    eProbeSensor_NTC,          // NTC传感器
}ProbeSensorType_te;// 探针传感器类型

typedef struct
{
	U16		u16CavitySensorVal;			    // 箱体传感器数据
	U16		u16SteamGeneratorSensorVal;	    // 蒸汽发生器传感器数据
	U16		u16BottomSensorVal;			    // 底部传感器数据
	U16		u16ProbeSensorVal;			    // 肉感探针数据
	U16		u16OxygenSensorVal;			    // 氧传感器数据
    
    CavitySensorType_te eCavitySensorType;  // 腔体温度传感器类型
    ProbeSensorType_te  eProbeSensorType;   // 探针温度传感器类型
	
    SensorErr_te eCavitySensorErr;		    // 箱体传感器故障类型
	SensorErr_te eSteamSensorErr;		    // 蒸汽发生器传感器故障类型
	SensorErr_te eBottomSensorErr;		    // 底部传感器故障类型
	SensorErr_te eProbeSensorErr;		    // 探针传感器故障类型
	
	Bool	     bPullupResistor;           // 腔体温度传感器上拉电阻选择 0:470K,1:9.792K
    
	Timer_ts sTimer;     				    // 定时器
}SensorCtrl_ts; // 传感器控制

U8  Sensor_GetCavitySensorErr(void);                // 获取腔体传感器故障状态
U8  Sensor_GetSteamGeneratorSensorErr(void);        // 获取蒸汽发生器故障状态
U8  Sensor_GetBottomSensorErr(void);                // 获取底部传感器故障状态
U8  Sensor_GetProbeSensorErr(void);                 // 获取食物探针故障状态
U16 Sensor_GetCavitySensorVal(void);                // 获取箱体传感器数据
U16 Sensor_GetSteamGeneratorSensorVal(void);        // 获取蒸汽发生器数据
U16 Sensor_GetBottomSensorVal(void);                // 获取底部传感器数据
U16 Sensor_GetProbeSensorVal(void);                 // 获取食物探针数据
U16 Sensor_GetOxygenSensorVal(void);                // 获取氧气传感器数据
void Sensor_SetOxygenSensorVal(U16 u16OxygenVal);   // 保存氧气传感器数据
void Sensor_Init(void);                             // 传感器初始化

#endif

