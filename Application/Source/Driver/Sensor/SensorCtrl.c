/*******************************************************************************
  * 文件：SensorCtrl.c
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-20
  * 说明：传感器驱动
*******************************************************************************/
#include "SensorCtrl.h"
#include "NTC.h"
#include "PT1000.h"
#include "OxygenSensor.h"
#include "Hardware_ADC.h"
#include "Hardware_IO.h"
#include "Debug.h"
#include <string.h>

static SensorCtrl_ts sSensorCtrl;
U8 Sensor_GetCavitySensorErr(void)
{
	return sSensorCtrl.eCavitySensorErr;
}

U8 Sensor_GetSteamGeneratorSensorErr(void)
{
	return sSensorCtrl.eSteamSensorErr;
}

U8 Sensor_GetBottomSensorErr(void)
{
	return sSensorCtrl.eBottomSensorErr;
}

U8 Sensor_GetProbeSensorErr(void)
{
	return sSensorCtrl.eProbeSensorErr;
}

/*******************************************************************************
  * 函数名：Sensor_GetCavitySensorVal
  * 功  能：获取箱体温度传感器温度
  * 参  数：无
  * 返回值：U16 u16CavitySensorVal：箱体温度传感器温度
  * 说  明：无
*******************************************************************************/
U16 Sensor_GetCavitySensorVal(void)
{
	return sSensorCtrl.u16CavitySensorVal;
}

/*******************************************************************************
  * 函数名：Sensor_GetBottomSensorVal
  * 功  能：获取底部温度传感器温度
  * 参  数：无
  * 返回值：U16 u16BottomSensorVal：底部温度传感器温度
  * 说  明：无
*******************************************************************************/
U16 Sensor_GetSteamGeneratorSensorVal(void)
{
	return sSensorCtrl.u16SteamGeneratorSensorVal;
}

/*******************************************************************************
  * 函数名：Sensor_GetBottomSensorVal
  * 功  能：获取底部温度传感器温度
  * 参  数：无
  * 返回值：U16 u16BottomSensorVal：底部温度传感器温度
  * 说  明：无
*******************************************************************************/
U16 Sensor_GetBottomSensorVal(void)
{
	return sSensorCtrl.u16BottomSensorVal;
}

/*******************************************************************************
  * 函数名：Sensor_GetProbeSensorVal
  * 功  能：获取食物探针温度
  * 参  数：无
  * 返回值：U16 u16ProbeSensorVal：探针温度
  * 说  明：无
*******************************************************************************/
U16 Sensor_GetProbeSensorVal(void)
{
	return sSensorCtrl.u16ProbeSensorVal;
}

/*******************************************************************************
  * 函数名：Sensor_GetOxygenSensorVal
  * 功  能：获取氧气传感器浓度
  * 参  数：无
  * 返回值：U16 u16OxygenVal：氧气浓度
  * 说  明：无
*******************************************************************************/
U16 Sensor_GetOxygenSensorVal(void)
{
	return sSensorCtrl.u16OxygenSensorVal;
}

/*******************************************************************************
  * 函数名：Sensor_SetOxygenSensorVal
  * 功  能：设置氧气传感器浓度
  * 参  数：U16 u16OxygenVal：氧气浓度
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void Sensor_SetOxygenSensorVal(U16 u16OxygenVal)
{
    sSensorCtrl.u16OxygenSensorVal = u16OxygenVal;
}

/*******************************************************************************
  * 函数名：SensorCheckHook_100MS
  * 功  能：传感器定时100ms采集回调函数
  * 参  数：无
  * 返回值：无
  * 说  明：定时100ms转换一次，采集20次ADC，去掉最大最小值取平均。查表求温度。
*******************************************************************************/
static void SensorCheckHook_100MS(void)
{
	U8  u8Index;
	U16 au16ADCTemp[20];
	U16 u16ADC_SUM = 0;
    U16 u16ADC_AVE = 0;
	U16 u16ADC_MAX = 0;
	U16 u16ADC_MIN = 0xFFFF;
	
	// 采集蒸发盘NTC
	memset(au16ADCTemp,0x0000,sizeof(au16ADCTemp));
	for(u8Index = 0;u8Index < 20;u8Index++)
	{
		// 连续采集20次
		au16ADCTemp[u8Index] = Hardware_GetSteamPanNTC(); 

		// 记录最小值
		if(au16ADCTemp[u8Index] < u16ADC_MIN)
		{
			u16ADC_MIN = au16ADCTemp[u8Index];
		}

		// 记录最大值
		if(au16ADCTemp[u8Index] > u16ADC_MAX)
		{
			u16ADC_MAX = au16ADCTemp[u8Index];
		}

		// 求和
		u16ADC_SUM += au16ADCTemp[u8Index];
	}

	// 减掉最大最小值，求平均
	u16ADC_SUM -= u16ADC_MAX;
	u16ADC_SUM -= u16ADC_MIN;
    u16ADC_AVE = u16ADC_SUM/18;
    
	// 根据平均值查表求温度
	if(u16ADC_AVE >= U16_SENSOR_OPEN_ADC_MAX)
	{
		sSensorCtrl.eSteamSensorErr = eSENSOR_ERR_OPEN;
		sSensorCtrl.u16SteamGeneratorSensorVal = 0;
	}
	else if(u16ADC_AVE <= U16_SENSOR_SHORT_ADC_MIN)
	{
		sSensorCtrl.eSteamSensorErr = eSENSOR_ERR_SHORT;
		sSensorCtrl.u16SteamGeneratorSensorVal = 0;
	}
	else
	{
		sSensorCtrl.eSteamSensorErr = eSENSOR_ERR_NO;
		sSensorCtrl.u16SteamGeneratorSensorVal = NTC_GetTemperFromNTCTable(u16ADC_AVE);
	}

	// 采集底部NTC
	u16ADC_SUM = 0;
    u16ADC_AVE = 0;
	u16ADC_MAX = 0;
	u16ADC_MIN = 0xFFFF;
	memset(au16ADCTemp,0x0000,sizeof(au16ADCTemp));
	for(u8Index = 0;u8Index < 20;u8Index++)
	{
		// 连续采集20次
		au16ADCTemp[u8Index] = Hardware_GetBottomNTC(); 

		// 记录最小值
		if(au16ADCTemp[u8Index] < u16ADC_MIN)
		{
			u16ADC_MIN = au16ADCTemp[u8Index];
		}

		// 记录最大值
		if(au16ADCTemp[u8Index] > u16ADC_MAX)
		{
			u16ADC_MAX = au16ADCTemp[u8Index];
		}

		// 求和
		u16ADC_SUM += au16ADCTemp[u8Index];
	}

	// 减掉最大最小值
	u16ADC_SUM -= u16ADC_MAX;
	u16ADC_SUM -= u16ADC_MIN;
    u16ADC_AVE = u16ADC_SUM/18;
    
	if(u16ADC_AVE >= U16_SENSOR_OPEN_ADC_MAX)
	{
		sSensorCtrl.eBottomSensorErr = eSENSOR_ERR_OPEN;
		sSensorCtrl.u16BottomSensorVal = 0;
	}
	else if(u16ADC_AVE <= U16_SENSOR_SHORT_ADC_MIN)
	{
		sSensorCtrl.eBottomSensorErr = eSENSOR_ERR_SHORT;
		sSensorCtrl.u16BottomSensorVal = 0;
	}
	else
	{
		sSensorCtrl.eBottomSensorErr = eSENSOR_ERR_NO;
		sSensorCtrl.u16BottomSensorVal = NTC_GetTemperFromNTCTable(u16ADC_AVE);	
	}

	// 采集肉感探针温度
	u16ADC_SUM = 0;
    u16ADC_AVE = 0;
	u16ADC_MAX = 0;
	u16ADC_MIN = 0xFFFF;
	memset(au16ADCTemp,0x0000,sizeof(au16ADCTemp));
	for(u8Index = 0;u8Index < 20;u8Index++)
	{
		// 连续采集20次
		au16ADCTemp[u8Index] = Hardware_GetProbeNTC(); 

		// 记录最小值
		if(au16ADCTemp[u8Index] < u16ADC_MIN)
		{
			u16ADC_MIN = au16ADCTemp[u8Index];
		}

		// 记录最大值
		if(au16ADCTemp[u8Index] > u16ADC_MAX)
		{
			u16ADC_MAX = au16ADCTemp[u8Index];
		}

		// 求和
		u16ADC_SUM += au16ADCTemp[u8Index];
	}

	// 减掉最大最小值
	u16ADC_SUM -= u16ADC_MAX;
	u16ADC_SUM -= u16ADC_MIN;
    u16ADC_AVE  = u16ADC_SUM/18;

    if(u16ADC_AVE >= U16_SENSOR_OPEN_ADC_MAX)
	{
		sSensorCtrl.eProbeSensorErr = eSENSOR_ERR_OPEN;
		sSensorCtrl.u16ProbeSensorVal = 0;
	}
	else if(u16ADC_AVE <= U16_SENSOR_SHORT_ADC_MIN)
	{
		sSensorCtrl.eProbeSensorErr = eSENSOR_ERR_SHORT;
		sSensorCtrl.u16ProbeSensorVal = 0;
	}
	else
	{
		sSensorCtrl.eProbeSensorErr = eSENSOR_ERR_NO;
        
        // 根据传感器类型，查表求温度
        if(sSensorCtrl.eProbeSensorType == eProbeSensor_NTC)
        {
            // NTC查表
            sSensorCtrl.u16ProbeSensorVal = NTC_GetTemperFromProbeRTTable(u16ADC_AVE);	
        }
        else if(sSensorCtrl.eProbeSensorType == eProbeSensor_PT1000)
        {
            // PT1000查表
            sSensorCtrl.u16ProbeSensorVal = NTC_GetTemperFromPT1000Table(u16ADC_AVE);
        }
	}

    // 根据腔体传感器类型，选择采集NTC或PT1000
	if(sSensorCtrl.eCavitySensorType == eCavitySensor_NTC)
    {
        // 求取腔体NTC传感器温度
        u16ADC_SUM = 0;
        u16ADC_AVE = 0;
        u16ADC_MAX = 0;
        u16ADC_MIN = 0xFFFF;
        memset(au16ADCTemp,0x0000,sizeof(au16ADCTemp));
        for(u8Index = 0;u8Index < 20;u8Index++)
        {
            // 连续采集20次
            au16ADCTemp[u8Index] = Hardware_GetCavityNTC(); 

            // 记录最小值
            if(au16ADCTemp[u8Index] < u16ADC_MIN)
            {
                u16ADC_MIN = au16ADCTemp[u8Index];
            }

            // 记录最大值
            if(au16ADCTemp[u8Index] > u16ADC_MAX)
            {
                u16ADC_MAX = au16ADCTemp[u8Index];
            }

            // 求和
            u16ADC_SUM += au16ADCTemp[u8Index];
        }

        // 减掉最大最小值，求平均
        u16ADC_SUM -= u16ADC_MAX;
        u16ADC_SUM -= u16ADC_MIN;
        u16ADC_AVE = u16ADC_SUM/18;

        if(u16ADC_AVE >= U16_SENSOR_OPEN_ADC_MAX)
        {
            // 开路故障
            sSensorCtrl.eCavitySensorErr = eSENSOR_ERR_OPEN;
            sSensorCtrl.u16CavitySensorVal = 0;
        }
        else if(u16ADC_AVE <= U16_SENSOR_SHORT_ADC_MIN)
        {
            // 短路故障
            sSensorCtrl.eCavitySensorErr = eSENSOR_ERR_SHORT;
            sSensorCtrl.u16CavitySensorVal = 0;
        }
        else
        {
            // 无故障，查表求温度
            sSensorCtrl.eCavitySensorErr = eSENSOR_ERR_NO;

            if(sSensorCtrl.bPullupResistor == 0)
            {
                // 从表1查温度，分压电阻为470K
                sSensorCtrl.u16CavitySensorVal = NTC_GetTemperFromCavityNTCRTTable1(u16ADC_AVE);

                // 温度超过150度，切换分压电阻
                if(sSensorCtrl.u16CavitySensorVal >= 150)
                {
                    sSensorCtrl.bPullupResistor = 1;
                    Hardware_NTCSelxCtrl(OFF);
                }
            }
            else if(sSensorCtrl.bPullupResistor == 1)
            {
                // 从表2查温度，分压电阻9.792k
                sSensorCtrl.u16CavitySensorVal = NTC_GetTemperFromCavityNTCRTTable2(u16ADC_AVE);

                // 温度低于140度，切换分压电阻
                if(sSensorCtrl.u16CavitySensorVal <= 140)
                {
                    sSensorCtrl.bPullupResistor = 0;
                    Hardware_NTCSelxCtrl(ON);
                }
            }	
        }
    }
    else if(sSensorCtrl.eCavitySensorType == eCavitySensor_PT1000)
    {
        // 求取腔体PT1000传感器温度
        u16ADC_SUM = 0;
        u16ADC_AVE = 0;
        u16ADC_MAX = 0;
        u16ADC_MIN = 0xFFFF;
        memset(au16ADCTemp,0x0000,sizeof(au16ADCTemp));
        for(u8Index = 0;u8Index < 20;u8Index++)
        {
            // 连续采集20次
            au16ADCTemp[u8Index] = Hardware_GetCavityNTC(); 

            // 记录最小值
            if(au16ADCTemp[u8Index] < u16ADC_MIN)
            {
                u16ADC_MIN = au16ADCTemp[u8Index];
            }

            // 记录最大值
            if(au16ADCTemp[u8Index] > u16ADC_MAX)
            {
                u16ADC_MAX = au16ADCTemp[u8Index];
            }

            // 求和
            u16ADC_SUM += au16ADCTemp[u8Index];
        }

        // 减掉最大最小值，求平均
        u16ADC_SUM -= u16ADC_MAX;
        u16ADC_SUM -= u16ADC_MIN;
        u16ADC_AVE = u16ADC_SUM/18;

        if(u16ADC_AVE >= U16_SENSOR_OPEN_ADC_MAX)
        {
            // 开路故障
            sSensorCtrl.eCavitySensorErr = eSENSOR_ERR_OPEN;
            sSensorCtrl.u16CavitySensorVal = 0;
        }
        else if(u16ADC_AVE <= U16_SENSOR_SHORT_ADC_MIN)
        {
            // 短路故障
            sSensorCtrl.eCavitySensorErr = eSENSOR_ERR_SHORT;
            sSensorCtrl.u16CavitySensorVal = 0;
        }
        else
        {
            // 无故障，查表求温度
            sSensorCtrl.eCavitySensorErr = eSENSOR_ERR_NO;
            sSensorCtrl.u16CavitySensorVal = NTC_GetTemperFromPT1000Table(u16ADC_AVE);	
        }
    }

}

/*******************************************************************************
  * 函数名：Sensor_Init
  * 功  能：传感器采集初始化
  * 参  数：无
  * 返回值：无
  * 说  明：初始化定时器，定时采集ADC
*******************************************************************************/
void Sensor_Init(void)
{
    // 检测Selx电平，高电平NTC，低电平PT1000、
    if(1 == Hardware_SelxCheck())
    {
        // 高电平NTC
        sSensorCtrl.eCavitySensorType = eCavitySensor_NTC;
        // 上电默认上拉电阻选择470K
        sSensorCtrl.bPullupResistor = 0;
        // 输出高电平，MOS截止，470K电阻与NTC分压
        Hardware_NTCSelxCtrl(ON);
    }
    else if(0 == Hardware_SelxCheck())
    {
        // 低电平PT1000
        sSensorCtrl.eCavitySensorType = eCavitySensor_PT1000;
    }
    
    // 检测探针判断电平，高电平NTC，低电平PT1000
    if(1 == Hardware_ProbTypeCheck())
    {
        // 高电平NTC
        sSensorCtrl.eProbeSensorType = eProbeSensor_NTC;
    }
    else if(0 == Hardware_ProbTypeCheck())
    {
        // 低电平PT1000
        sSensorCtrl.eProbeSensorType = eProbeSensor_PT1000;
    }
    
	// 故障类型默认无故障
	sSensorCtrl.eBottomSensorErr = eSENSOR_ERR_NO;
	sSensorCtrl.eCavitySensorErr = eSENSOR_ERR_NO;
	sSensorCtrl.eSteamSensorErr  = eSENSOR_ERR_NO;
	sSensorCtrl.eProbeSensorErr  = eSENSOR_ERR_NO;

    // 初始化氧气传感器
    OxygenSensor_Init();
    
	// 定时100ms采集一次温度值
    OS_TimerStart(&sSensorCtrl.sTimer,100,SensorCheckHook_100MS);
}
	
