/*******************************************************************************
  * 文件：InverterCtrl.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2022-03-01
  * 说明：变频微波控制模块
*******************************************************************************/
#ifndef _INVERTER_CTRL_H
#define _INVERTER_CTRL_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"
#include "StateMachine.h"
#include "OS_Timer.h"

/* 宏定义 *********************************************************************/
#define U8_INVERTER_MOTIVE_1_DUTY       ((U8)   85)     // Motive1状态占空比
#define U8_INVERTER_MOTIVE_2_DUTY       ((U8)   60)     // Motive2状态占空比
#define U8_INVERTER_RUN_MAX_DUTY        ((U8)   85)     // Run状态最大占空比
#define U8_INVERTER_RUN_MIN_DUTY        ((U8)   20)     // Run状态最小占空比

/* 类型定义 *******************************************************************/
typedef enum
{
    eINVERTER_OFF = 0,       // 关闭
    eINVERTER_INIT,          // 初始化
    eINVERTER_MOTIVE_1,      // motive 1
    eINVERTER_MOTIVE_2,      // motive 2
    eINVERTER_RUN,           // run
    eINVERTER_ERROR,         // 故障
}InverterState_te; // 变频器工作状态

typedef union
{
    U8 u8Error;
    struct 
    {
        U8 u1ErrorH90 : 1;
        U8 u1ErrorH95 : 1;
        U8 u1ErrorH96 : 1;
        U8 u1ErrorH97 : 1;
        U8 u1ErrorH98 : 1;
        U8 u1ErrorH99 : 1;
        U8 u1ErrorU65 : 1;
        U8 u1Reserve  : 1;
    }uError;
}InverterError_tu;  // 故障码

typedef struct
{
    U8                  u8Duty;         // 输出占空比
    U8                  u8ErrorH90Times;// H90故障出现次数（30次报警）
    U8                  u8ErrorH95Times;// H95故障出现次数（三次报故障）
    U8                  u8ErrorH96Times;// H95故障出现次数（三次报故障）
    U8                  u8ErrorH97Times;// H97故障出现次数（五次报故障）
    U8                  u8ErrorH98Times;// H98故障出现次数（三次报故障）
    InverterError_tu    uError;         // 故障码
    StateMachine_ts     sInverterSM;    // 状态机
}InverterCtrl_ts;

/* 函数声明 ------------------------------------------------------------------*/
U8 Inverter_GetError(void);             // 获取变频器故障码
U8 Inverter_GetDuty(void);              // 获取变频器占空比
void Inverter_ChangeDuty(U8 u8Duty);    // 变频器修改占空比
void Inverter_TimerIntHandler(void);    // 变频器500us定时器中断处理函数
void Inverter_Init(void);               // 变频器初始化

#endif  /*** #ifndef __UARTCOMM_DRIVER_H ***/

/***************************** 文件结束 ***************************************/
