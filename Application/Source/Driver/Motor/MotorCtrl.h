/*******************************************************************************
  * 文件：MotorCtrl.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-20
  * 说明：面板翻转电机驱动
*******************************************************************************/
#ifndef _MOTOR_CTRL_H
#define _MOTOR_CTRL_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"
#include "StateMachine.h"
#include "OS_Timer.h"

/* 宏定义 *********************************************************************/
#define U8_MOTOR_SPEED_FORWARD_DUTY     ((U8)  100) // 翻转电机正转转速转空比（0-100）
#define U8_MOTOR_SPEED_REVERSE_DUTY     ((U8)  85)  // 翻转电机反转转速转空比（0-100）
#define U16_MOTOR_FORWARD_BLOCK_ADC     ((U16) 500) // 电机正转堵转时ADC上限阈值
#define U16_MOTOR_REVERSE_BLOCK_ADC     ((U16) 180) // 电机反转堵转时ADC上限阈值
#define U8_MOTOR_BLOCK_OVER_TIME        ((U8)  20)  // 电机堵转超时判断时间，单位：10ms

/* 类型定义 *******************************************************************/
typedef enum
{
    eMOTOR_CLOSED  = 1,         // 完全关闭
    eMOTOR_OPENED  = 2,         // 完全打开
    eMOTOR_CLOSING = 3,         // 正在关闭
    eMOTOR_OPENING = 4,         // 正在打开
    eMOTOR_CLOSING_PAUSE = 5,   // 关闭时停止
    eMOTOR_OPENING_PAUSE = 6,   // 打开时停止
}MotorState_te;// 翻转电机运行状态

U8   Motor_GetState(void);  // 获取翻转电机状态
void Motor_ChangeState(MotorState_te eMotorState); // 切换翻转电机状态
void Motor_Init(void);      // 翻转电机初始化

#endif
