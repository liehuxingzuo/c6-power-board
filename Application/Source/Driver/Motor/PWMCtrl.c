/*******************************************************************************
  * 文件：PWMCtrl.c
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-20
  * 说明：PWM模拟
*******************************************************************************/
#include "PWMCtrl.h"
#include "Hardware_Timer.h"

/*******************************************************************************
  * 函数名：PWM_IN1Ctrl
  * 功  能：电机INT1控制
  * 参  数：U8 u8Duty：占空比 0-100
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void PWM_IN1Ctrl(U8 u8Duty)
{
    Hardware_CH2_PWMChangeDuty(u8Duty);
}

/*******************************************************************************
  * 函数名：PWM_IN2Ctrl
  * 功  能：电机INT2控制
  * 参  数：U8 u8Duty：占空比 0-100
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void PWM_IN2Ctrl(U8 u8Duty)
{
    Hardware_CH1_PWMChangeDuty(u8Duty);
}

