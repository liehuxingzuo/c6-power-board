/*******************************************************************************
  * 文件：UserInit.c
  * 作者：djy
  * 版本：v1.1.0
  * 日期：2019-05-15
  * 说明：应用层和驱动初始化
*******************************************************************************/
#include "OS_User.h"
#include "Heartbeat.h"
#include "Serial.h"
#include "RelayCtrl.h"
#include "MotorCtrl.h"
#include "SensorCtrl.h"
#include "InverterCtrl.h"
#include "UartComm.h"
#include "printk.h"

/* 变量定义 *******************************************************************/
// 用户初始化函数表
const InitHandler_tpf apfUserInitHandlerTable[] =
{
    Serial_Init,        // 串口驱动初始化
    printk_init,        // printk打印初始化
    RelayCtrl_Init,     // 继电器控制初始化
    Motor_Init,         // 翻转电机控制初始化
    Inverter_Init,      // 变频器初始化
    Sensor_Init,        // 传感器采集初始化
    UartComm_Init,      // 串口通信初始化
    //Heartbeat_Init,     // 心跳测试初始化
    NULL               // NULL结尾
};

