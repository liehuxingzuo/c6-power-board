/*******************************************************************************
  * 文件：Heartbeat.c
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-05-16
  * 说明：心跳
*******************************************************************************/
/* 头文件 *********************************************************************/
#include "Heartbeat.h"
#include "OS_Timer.h"
#include "Hardware_IO.h"
#include "RelayPower.h"
#include "Serial.h"
#include "SensorCtrl.h"
#include "Debug.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量定义 *******************************************************************/
static Timer_ts sHeartbeatTimer;    // 心跳定时器

/* 函数声明 *******************************************************************/
static void HeartbeatTimerCallback(void);    // 心跳定时器回调函数

/* 函数定义 *******************************************************************/
/*******************************************************************************
  * 函数名：Heartbeat_Init
  * 功  能：初始化
  * 参  数：无
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void Heartbeat_Init(void)
{
  	// 开启零总和12V
//	RelayZeroLine1Ctrl(ON);
//	RelayZeroLine2Ctrl(ON);
//	Relay12VolCtrl(ON);
    // 开启心跳定时器
	OS_TimerStart(&sHeartbeatTimer, 1000, HeartbeatTimerCallback);
}

/*******************************************************************************
  * 函数名：HeartbeatTimerCallback
  * 功  能：心跳定时器回调函数
  * 参  数：无
  * 返回值：无
  * 说  明：无
*******************************************************************************/
static void HeartbeatTimerCallback(void)
{
  
    DEBUG("Heart Beat Test:%d\r\n",OS_TimerGetExpiredTimes(&sHeartbeatTimer));
	DEBUG("CavitySensor :%d\r\n",Sensor_GetCavitySensorVal());
	DEBUG("SteamPanSensor :%d\r\n",Sensor_GetSteamGeneratorSensorVal());
	DEBUG("BottomSensor :%d\r\n",Sensor_GetBottomSensorVal());
	DEBUG("ProbeSensor :%d\r\n",Sensor_GetProbeSensorVal());
}

/***************************** 文件结束 ***************************************/
