/*******************************************************************************
  * 文件：ComFun.h
  * 作者：djy
  * 版本：v1.1.0
  * 日期：2019-05-15
  * 说明: 通用函数头文件
*******************************************************************************/
#ifndef _COMFUN_H
#define _COMFUN_H

// 通用头文件
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"

// 位操作
#define SET_BIT(x, bit) 		((x) |= 1 << (bit))
#define CLR_BIT(x, bit) 		((x) &= ~(1 << (bit)))
#define TST_BIT(x, bit) 		((x) & (1 << (bit)))
#define NOT_BIT(x, bit)			{							\
									if(TST_BIT(x,bit))		\
									{						\
										CLR_BIT(x, bit);	\
									}						\
									else					\
									{						\
										SET_BIT(x, bit);	\
									}						\
								}
#define GET_BITS(val,x1,x2) 	(((val)>>(x1))&((1<<((x2)-(x1)+1))-1))

#ifndef MIN
#define MIN(a, b) ((a)<(b) ? (a):(b))
#endif
#ifndef MAX
#define MAX(a, b) ((a)>(b) ? (a):(b))
#endif

#define ABS(a) ((a) < 0 ? -(a) : (a))
#define BCD2BIN(val) (((val) & 0x0f) + ((val) >> 4) * 10)
#define BIN2BCD(val) ((((val) / 10) << 4) + (val) % 10)

// 获取结构体中成员变量MEMBER相对于结构体的地址偏移量
#define offset_of(TYPE, MEMBER) \
        ((unsigned long) &((TYPE *)0)->MEMBER)

// 获取结构体成员所在的结构体变量的地址          
#define container_of(ptr, type, member) \
        ((type *)((char *)(ptr) - (unsigned long)(&((type *)0)->member)))

U8   CheckSum(const void *data, U16 len);
U32  Get_LittleEndian_Value(const U8 *p, int bytes);
U32  Get_BigEndian_Value(const U8 *p, int bytes);
void Put_LittleEndian_Val(U32 val, U8 *p, int bytes);
void Put_BigEndian_Val(U32 val, U8 *p, int bytes);
void BubbleSort(U16 buff[],U16 u16Len,U8 u8Order);
void Hex2Bcd(U32 value, U8 *bcd, U8 bytes);
U32  Bcd2Hex(U8 *bcd, U8 bytes);
U32  Is_All_XX(const void *s1, U8 val, U32 n);

#endif

