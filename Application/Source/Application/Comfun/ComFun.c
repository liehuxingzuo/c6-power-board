#include "ComFun.h"

/*******************************************************************************
  * 函数名: CheckSum
  * 功  能  ：求和校验
  * 参  数 ：const void *p：字节数据的存储地址
  *			int len：数据长度
  * 返回值：求和
  * 说  明 ：无
*******************************************************************************/
U8 CheckSum(const void *data, U16 len)
{
    U8 cs = 0;

    while (len-- > 0)
        cs += *((U8 *) data + len);
    return cs;
}

/*******************************************************************************
  * 函数名: Get_LittleEndian_Value
  * 功  能  ：获取小端数据
  * 参  数 ：const U8 *p：字节数据的存储地址
  *			 int bytes：数据长度
  * 返回值：转换为小端的数据
  * 说  明 ：无
*******************************************************************************/
U32 Get_LittleEndian_Value(const U8 *p, int bytes)
{
    U32 ret = 0;

    while (bytes-- > 0)
    {
        ret <<= 8;
        ret |= *(p + bytes);
    }
    return ret;
}

/*******************************************************************************
  * 函数名: Get_BigEndian_Value
  * 功  能  ：获取大端数据
  * 参  数 ：const U8 *p：字节数据的存储地址
  *			 int bytes：数据长度
  * 返回值：转换为大端的数据
  * 说  明 ：无
*******************************************************************************/
U32 Get_BigEndian_Value(const U8 *p, int bytes)
{
    U32 ret = 0;
    while (bytes-- > 0)
    {
        ret <<= 8;
        ret |= *p++;
    }

    return ret;
}

/*******************************************************************************
  * 函数名: Put_LittleEndian_Val
  * 功  能  ：将数据小端存放
  * 参  数 ：U32 val：将要存放的数据
  *		     U8 *p：数据的存储地址
  *			 int bytes：数据长度
  * 返回值：将数据按小端模式存放在指定地址处
  * 说  明 ：无
*******************************************************************************/
void Put_LittleEndian_Val(U32 val, U8 *p, int bytes)
{
    while (bytes-- > 0)
    {
        *p++ = val & 0xFF;
        val >>= 8;
    }
}

/*******************************************************************************
  * 函数名: Put_BigEndian_Val
  * 功  能  ：将数据大端存放
  * 参  数 ：U32 val：将要存放的数据
  *		     U8 *p：数据的存储地址
  *			 int bytes：数据长度
  * 返回值：将数据按小端模式存放在指定地址处
  * 说  明 ：无
*******************************************************************************/
void Put_BigEndian_Val(U32 val, U8 *p, int bytes)
{
    while (bytes-- > 0)
    {
        *(p + bytes) = val & 0xFF;
        val >>= 8;
    }
}

/*******************************************************************************
  * 函数名: BubbleSort
  * 功  能  ：冒泡排序
  * 参  数 ：void* buff:数据存放地址
  *		    U16   u16Len:数据长度
  *		    U8    u8Order：顺序 0：从大到小           1：从小到大
  * 返回值：冒泡排序
  * 说  明 ：无
*******************************************************************************/
void BubbleSort(U16 buff[],U16 u16Len,U8 u8Order)
{
    U16 i,j;
    U32 u32Temp;

    for(i=0;i<u16Len;i++)
    {
        for(j=0;j<u16Len-i;j++)
        {
            if(u8Order)
            {
                //从小到大排序
                if(buff[j] > buff[j+1])
                {
                    u32Temp = buff[j];
                    buff[j] = buff[j+1];
                    buff[j+1] = u32Temp;
                }
            }
            else
            {
                //从大到小排序
                if(buff[j] < buff[j+1])
                {
                    u32Temp = buff[j];
                    buff[j] = buff[j+1];
                    buff[j+1] = u32Temp;
                }
            }
        }
    }
}

void Hex2Bcd(U32 value, U8 *bcd, U8 bytes)
{
    U8 x;

    if (bytes > 5)
    {
        bytes = 5;
    }
    
    while (bytes--)
    {
        x = value % 100u;
        *bcd = BIN2BCD(x);
        bcd++;
        value /= 100u;
    }
}

U32 Bcd2Hex(U8 *bcd, U8 bytes)
{
    U32 ret = 0;

    if (bytes > 4)
    {
        bytes = 4;
    }
    
    while (bytes-- > 0)
    {
        ret *= 100u;
        ret += BCD2BIN(bcd[bytes]);
    }
    return ret;
}

U32 Is_All_XX(const void *s1, U8 val, U32 n)
{
    while (n && *(U8 *) s1 == val)
    {
        s1 = (U8 *) s1 + 1;
        n--;
    }
    return !n;
}

