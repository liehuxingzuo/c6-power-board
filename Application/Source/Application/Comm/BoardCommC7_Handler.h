/*******************************************************************************
  * 文件：BoardCommC7_Handler.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2019-05-15
  * 说明: 显示板与电源板C7通信协议，C7协议为海尔内部协议
*******************************************************************************/
#ifndef _BOARD_COMM_C7_HANDLER_H
#define _BOARD_COMM_C7_HANDLER_H

// 通用头文件
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"  

#pragma pack(1)
typedef struct
{
    // Byte0
    U8  bUpOutHeatTubeCtrl   : 1;   // 上外加热管控制
    U8  bUpInHeatTubeCtrl    : 1;   // 上内加热管控制
    U8  bDownHeatTubeCtrl    : 1;   // 下加热管控制
    U8  bBackHeatTubeCtrl    : 1;   // 后加热管控制
    U8  bSteamPanCtrl        : 1;   // 蒸发盘控制（无此继电器）
    U8  bOutsideSteamPanCtrl : 1;   // 蒸发盘外管控制
    U8  bInsideSteamPanCtrl  : 1;   // 蒸发盘内管控制
    U8  bBackFan1Ctrl      	 : 1;  	// 背部风扇+控制

    //  Byte1
    U8  bCoolFan1Ctrl        : 1;   // 冷却风机1控制（散热风机）
    U8  bCoolFan2Ctrl        : 1;   // 冷却风机2控制
    U8  bDoorLockMotor       : 1;   // 门锁电机控制
    U8  bForkMotorCtrl       : 1;   // 烧叉电机控制
    U8  bWaxMotorCtrl        : 1;   // 蜡马达控制
    U8  bLampCtrl            : 1;   // 炉灯控制
    U8  bLiftingMotorCtrl    : 2;   // 升降电机控制（00=忽略， 01=面板升起， 10=面板降下）

    // Byte2
    U8  bZeroLine1Ctrl       : 1;   // 零线总控1
    U8  bZeroLine2Ctrl       : 1;   // 零线总控2
    U8  bIntakePumpCtrl      : 1;   // 进水泵控制
    U8  bDischargePumpCtrl   : 1;   // 排水泵控制
    U8  bZeroLine3Ctrl       : 1;   // 零线总控3
    U8  bClkValid            : 1;   // 时钟有效位
    U8  bReserve1            : 2;   // 保留1

    // Byte3
    U8  bVideoBoardPowerCtrl : 1;   // 视频板电源控制
    U8  bCameraCoolingFanCtrl: 1;   // 摄像头散热风扇控制
    U8  bOpenDoorMotorCtrl   : 2;   // 开门电机控制（00=忽略， 01=门关闭命令， 10=门打开命令）
    U8  bSolenoidValveCtrl   : 1;   // 电磁阀控制
    U8  bOxygenSensorCtrl    : 1;   // 氧传感器控制
    U8  bBackFan2Ctrl        : 1;   // 背部风扇-控制
    U8  bReserveCtrl		 : 1;	// 预留继电器控制

    // Byte4
    U8 u8Reserve3            : 8;   // 保留3

    // Byte5
    U8 u8Reserve4            : 8;   // 保留4

    // Byte6
    U8 u8ClkHour             : 8;   // 小时

    // Byte7
    U8 u8ClkMinute           : 8;   // 分钟

    // Byte8
    U8 u8ClkSecond           : 8;   // 秒

    // Byte9
    U8 u8Reserve8            : 8;   // 保留
    
    // Byte10
    U8 u8Reserve5            : 8;   // 保留

    // Byte11
    U8 u8Reserve6            : 8;   // 保留

    // Byte12
    U8 u8Reserve7            : 8;   // 保留
}BoardCommRecvData_ts;// 电源板接收的数据域
#pragma pack()

void FrameDownwardHandler(const U8 au8Data[],U8 u8Len); // 下行报文处理

#endif

