/*******************************************************************************
  * 文件：BoardCommC7_Comm.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2019-05-15
  * 说明: 显示板与电源板C7通信协议，C7协议为海尔内部协议
*******************************************************************************/
#ifndef _BOARD_COMM_C7_COMM_H
#define _BOARD_COMM_C7_COMM_H

// 通用头文件
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"  
#include "OS_Timer.h"

// 常量宏定义
#define u8BOARD_COMM_C7_FRAME_MAX_LEN         ((U8)150)         // 最大长度
#define u8BOARD_COMM_C7_FRAME_STX             ((U8)0xFF)        // 报文帧头
#define u8BOARD_COMM_C7_FRAME_SURPLUS         ((U8)0x55)        // 冗余字符
#define u8BOARD_COMM_C7_FRAME_LEN_MIN         ((U8)12)          // 报文最小长度，12个字节

// 电控板地址
#define u16BOARD_COMM_C7_FRAME_DISPLAY_ADDR   ((U16)0x000001)   // 显示板/流程板地址
#define u16BOARD_COMM_C7_FRAME_POWER_ADDR     ((U16)0x000002)   // 电源板地址 

// 帧类型
#define u16BOARD_COMM_C7_DOWNWARD             ((U16)0x0002)     // 下行（显示板->电源板）
#define u16BOARD_COMM_C7_UPWARD               ((U16)0x0102)     // 上行（电源板->显示板）

#pragma pack(1)
typedef struct
{
    // Byte1-Byte18
    U8  u8MajorVersion        : 8;   // 主版本
    U8  u8MinorVersion        : 8;   // 次版本
    U8  u8RevisionVersion     : 8;   // 修订版本
    U8  u8HardwareType        : 8;   // 硬件类型
    U8  u8CavitySensor_H      : 8;   // 腔体温度传感器高字节
    U8  u8CavitySensor_L      : 8;   // 腔体温度传感器低字节
    U8  u8Reserve23           : 8;   // 保留
    U8  u8Reserve24           : 8;   // 保留
    U8  u8SteamGeneratorSensor_H : 8;// 蒸汽发生器传感器高字节
    U8  u8SteamGeneratorSensor_L : 8;// 蒸汽发生器传感器低字节
    U8  u8BottomSensor_H      : 8;   // 底部温度传感器高字节
    U8  u8BottomSensor_L      : 8;   // 底部温度传感器低字节
    U8  u8ProbeSensor1_H      : 8;   // 肉感探针1高字节
    U8  u8ProbeSensor1_L      : 8;   // 肉感探针1低字节
    U8  u8ProbeSensor2_H      : 8;   // 肉感探针2高字节
    U8  u8ProbeSensor2_L      : 8;   // 肉感探针2低字节
    U8  u8ProbeSensor3_H      : 8;   // 肉感探针3高字节
    U8  u8ProbeSensor3_L      : 8;   // 肉感探针3低字节

    // Byte19
    U8  bUpOutHeatTubeStatus  : 1;   // 上外加热管状态
    U8  bUpInHeatTubeStatus   : 1;   // 下内加热管状态
    U8  bDownHeatTubeStatus   : 1;   // 下加热管状态
    U8  bBackHeatTubeStatus   : 1;   // 后加热管状态
    U8  bSteamPanStatus       : 1;   // 蒸发盘状态
    U8  bOutsideSteamPanStatus: 1;   // 蒸发盘外管状态
    U8  bInsideSteamPanStatus : 1;   // 蒸发盘内管状态
    U8  bConvectFanStatus     : 1;   // 对流风机状态
    
    // Byte20
    U8  bCoolFan1Status       : 1;   // 冷却风机1状态（散热风机）
    U8  bCoolFan2Status       : 1;   // 冷却风机2状态
    U8  bDoorLockMotorStatus  : 1;   // 门锁电机状态
    U8  bForkMotorStatus      : 1;   // 烧叉电机状态
    U8  bWaxMotorStatus       : 1;   // 蜡马达状态
    U8  bLampStatus           : 1;   // 炉灯状态
    U8  bLiftingMotorStatus   : 2;   // 升降电机状态（00=忽略， 01=面板升起， 10=面板降下）

    // Byte21
    U8  bZeroLine1Status      : 1;   // 零线总控1状态
    U8  bZeroLine2Status      : 1;   // 零线总控2状态
    U8  bIntakePumpStatus     : 1;   // 进水泵控制状态
    U8  bDrainPumpStatus      : 1;   // 排水泵控制状态
    U8  bZeroLine3Status      : 1;   // 零线总控3状态
    U8  bReserve1             : 3;   // 保留1

    // Byte22
    U8  bVideoBoardStatus     : 1;   // 视频版电源开关状态
    U8  bCameraStatus         : 1;   // 摄像头
    U8  bDoorMotorStatus  	  : 2;   // 开门电机开关状态（00=门停止， 0x01=门正在关闭， 0x02=门正在打开）
    U8  bDCFStatus      	  : 1;   // 电磁阀状态
    U8  bOxygenPositionStatus : 1;   // 氧传感器在位状态，0=不在位， 1=在位
    U8  bOxygenWorkStatus     : 1;   // 氧传感器工作状态，0=不工作， 1=工作中
	U8	u8Reserve2			  : 1;	 // 保留2

    // Byte23
    U8  u8OxygenValH          : 8;   // 氧气浓度高字节

    // Byte24
    U8  u8OxygenValL          : 8;   // 氧气浓度低字节

    // Byte25
    U8  bDoorStatus           : 1;   // 门开关状态
    U8  bDoorLockStatus       : 1;   // 门锁开关状态
    U8  bWasteWaterStatus	  : 1;	 // 废水盒开关满状态
    U8  u8Reserve3	     	  : 1;	 // 保留3
    U8  bMotorUpStatus		  : 1;	 // 面板电机上状态 0到位 1没到位
	U8  bMotorDownStatus	  : 1;	 // 面板电机下状态 0到位 1没到位
	U8  u8Reserve4	     	  : 2;	 // 保留4

    // Byte26
    U8  WasteWaterPosStatus   : 1;   // 废水盒位置状态 0=在位， 1=不在位
    U8  bWaterBoxStatus       : 1;   // 清水盒位置状态
    U8  bWaterBoxEmptyStatus  : 1;   // 清水盒空状态
    U8  bSteamPanWaterStatus  : 1;   // 蒸发盘水位状态（高水位）
    U8  u4Reserve7            : 4;   // 保留7

    // Byte27
    U8  u2CavitySensor1Err    : 2;   // 腔体温度传感器故障状态，00=无故障， 0x01=短路故障， 0x02=开路故障
	U8	u2CavitySensor2Err	  : 2;	 // 腔体温度传感器故障状态，00=无故障， 0x01=短路故障， 0x02=开路故障
	U8	u2SteamPanSensorErr	  : 2;	 // 蒸汽发生器传感器故障状态，00=无故障， 0x01=短路故障， 0x02=开路故障
	U8	u2BottomSensorErr	  : 2;	 // 底部温度传感器故障状态，00=无故障， 0x01=短路故障， 0x02=开路故障

    // Byte28
    U8  u2Probe1Sensor1Err    : 2;   // 探针1温度传感器故障状态，00=无故障， 0x01=短路故障， 0x02=开路故障
	U8	u2Probe2Sensor1Err	  : 2;	 // 探针2温度传感器故障状态，00=无故障， 0x01=短路故障， 0x02=开路故障
	U8	u2Probe3Sensor1Err	  : 2;	 // 探针3温度传感器故障状态，00=无故障， 0x01=短路故障， 0x02=开路故障
	U8	u2Reserve8      	  : 2;	 // 预留

    // Byte29
    U8  u8Reserve10           : 8;   // 保留10，告警

    // Byte30
    U8  u8Reserve11           : 8;   // 保留11

    // Byte31
    U8  u8Reserve12           : 8;   // 保留12

    // Byte32
    U8  u8Reserve13           : 8;   // 保留13

    // Byte33
    U8  u8Reserve14           : 8;   // 保留14

    // Byte34
    U8  u8Reserve15           : 8;   // 保留15

    // Byte35
    U8  u8Reserve16           : 8;   // 保留16

    // Byte36
    U8  u8Reserve17           : 8;   // 保留17

    // Byte37
    U8  u8Reserve18           : 8;   // 保留18

    // Byte38
    U8  u8Reserve19           : 8;   // 保留19

    // Byte39
    U8  u8Reserve20           : 8;   // 保留20

    // Byte40
    U8  u8Reserve21           : 8;   // 保留21

    // Byte41
    U8  u8Reserve22           : 8;   // 保留22

	// Byte42校验位
    U8  u8CRC				  : 8 ;	 // CRC8校验位
}BoardCommSendData_ts;// 电源板上报的数据域

typedef struct
{
    U8 u8STX1;              // 帧头1
    U8 u8STX2;              // 帧头2
    U8 u8Len;               // 帧长
    U8 au8DestAddr[3];      // 目的地址
    U8 au8SourceAddr[3];    // 源地址
    U8 u8Cmd_H;             // 帧类型高字节
    U8 u8Cmd_L;             // 帧类型低字节
    U8 au8Data[];           // 数据
}BoardFrame_ts;//C7通信报文

#pragma pack()

typedef struct
{
    Timer_ts  sTimer;
}BoardCommCtrl_ts;// 底板通信控制

void BoardComm_Init(void);       // 底板通信初始化

#endif

