/*******************************************************************************
  * 文件：UartComm_Cmd.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：命令
*******************************************************************************/
#ifndef __UARTCOMM_CMD_H
#define __UARTCOMM_CMD_H

/* 头文件 --------------------------------------------------------------------*/
#include "UartComm_Typedefine.h"

/* 宏定义 --------------------------------------------------------------------*/
// 设备信息命令
#define u8UARTCOMM_CMD_BCST_PROT_INFO           ((U8) 0x00)    // 广播协议信息
#define u8UARTCOMM_CMD_REQ_INQUIRE_SW_VER       ((U8) 0x01)    // 请求查询软件版本
#define u8UARTCOMM_CMD_RES_INQUIRE_SW_VER       ((U8) 0x02)    // 响应查询软件版本
#define u8UARTCOMM_CMD_REQ_INQUIRE_SW_ID        ((U8) 0x03)    // 请求查询软件标识符
#define u8UARTCOMM_CMD_RES_INQUIRE_SW_ID        ((U8) 0x04)    // 响应查询软件标识符
// 通信机制命令
#define u8UARTCOMM_CMD_REQ_HEARTBEAT            ((U8) 0x11)    // 请求心跳
#define u8UARTCOMM_CMD_RES_HEARTBEAT            ((U8) 0x12)    // 响应心跳
#define u8UARTCOMM_CMD_REQ_SWITCH_MODE          ((U8) 0x13)    // 请求切换模式
#define u8UARTCOMM_CMD_RES_SWITCH_MODE          ((U8) 0x14)    // 响应切换模式
// OTA命令
#define u8UARTCOMM_CMD_OTA_REQ_NOTIFY_UPGRADE   ((U8) 0xA1)    // 请求通知升级
#define u8UARTCOMM_CMD_OTA_RES_NOTIFY_UPGRADE   ((U8) 0xA2)    // 响应通知升级
#define u8UARTCOMM_CMD_OTA_REQ_GET_FIRMWARE     ((U8) 0xA3)    // 请求获取固件
#define u8UARTCOMM_CMD_OTA_RES_GET_FIRMWARE     ((U8) 0xA4)    // 响应获取固件
#define u8UARTCOMM_CMD_OTA_REQ_FEEDBACK_RESULT  ((U8) 0xA5)    // 请求反馈结果
#define u8UARTCOMM_CMD_OTA_RES_FEEDBACK_RESULT  ((U8) 0xA6)    // 响应反馈结果
// DBA命令
#define u8UARTCOMM_CMD_PUBLISH_DBA              ((U8) 0xD0)    // 发布DBA
#define u8UARTCOMM_CMD_REQ_READ_DBA             ((U8) 0xD1)    // 请求读取DBA
#define u8UARTCOMM_CMD_RES_READ_DBA             ((U8) 0xD2)    // 响应读取DBA
#define u8UARTCOMM_CMD_REQ_WRITE_DBA            ((U8) 0xD3)    // 请求写入DBA
#define u8UARTCOMM_CMD_RES_WRITE_DBA            ((U8) 0xD4)    // 响应写入DBA
#define u8UARTCOMM_CMD_REQ_SUBSCRIBE_DBA        ((U8) 0xD5)    // 请求订阅DBA
#define u8UARTCOMM_CMD_RES_SUBSCRIBE_DBA        ((U8) 0xD6)    // 响应订阅DBA
#define u8UARTCOMM_CMD_REQ_BITS_WRITE_DBA       ((U8) 0xD7)    // 请求位写入DBA
#define u8UARTCOMM_CMD_RES_BITS_WRITE_DBA       ((U8) 0xD8)    // 响应位写入DBA
#define u8UARTCOMM_CMD_NOTIFY_DBA               ((U8) 0xDE)    // 通知DBA

/* 类型定义 ------------------------------------------------------------------*/
/* 变量定义 ------------------------------------------------------------------*/
/* 函数声明 ------------------------------------------------------------------*/

#endif    /*** #ifndef __UARTCOMM_CMD_H ***/

/***************************** 文件结束 ***************************************/
