/*******************************************************************************
  * 文件：UartComm_Device.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：设备
*******************************************************************************/
#ifndef __UARTCOMM_DEVICE_H
#define __UARTCOMM_DEVICE_H
/* 头文件 --------------------------------------------------------------------*/
#include "UartComm_Typedefine.h"

/* 宏定义 --------------------------------------------------------------------*/
#define u16LOAD_RO_DATA_ADDR    ((U16) 0x4000)    // 负载只读数据地址
#define u16LOAD_RW_DATA_ADDR    ((U16) 0x4100)    // 负载读写数据地址
#define u16LOAD_WO_DATA_ADDR    ((U16) 0x4200)    // 负载只写数据地址

/* 类型定义 ------------------------------------------------------------------*/
// 传感器
typedef struct
{
    U16 u6ValueMsb : 6;    // 数据高位
    U16 u2State    : 2;    // 状态
    U16 u8ValueLsb : 8;    // 数据低位
} Sensor_ts;

// 负载只读数据
typedef struct
{
    // byte 0x00-0x01
    Sensor_ts sCavityPrimarySensor;      // 腔体主传感器
    // byte 0x02-0x03
    Sensor_ts sCavitySecondarySensor;    // 腔体副传感器
    // byte 0x04-0x05
    Sensor_ts sSteamerSensor;            // 蒸发器传感器
    // byte 0x06-0x07
    Sensor_ts sBottomSensor;             // 底部传感器
    // byte 0x08-0x09
    Sensor_ts sMeatProbeSensor;          // 肉感探针传感器
    // byte 0x0A-0x0B
    Sensor_ts sOxygenSensor;             // 氧传感器
    // byte 0x0C
    U8 u3Reserve1                : 3;    // 预留
    U8 u1DoorOpenCloseState      : 1;    // 门开关状态
    U8 u1SteamerFullState        : 1;    // 蒸发器满状态
    U8 u1RestWaterBoxFullState   : 1;    // 余水盒满状态
    U8 u1WasteWaterBoxFullState  : 1;    // 废水盒满状态
    U8 u1WasteWaterBoxExistState : 1;    // 废水盒在位状态
    // byte 0x0D
    U8 u3Reserve2                : 3;    // 预留
    U8 u3LifitingPanelState      : 3;    // 升降面板状态
    U8 u1LoadBoardCommRecvError  : 1;    // 负载板通信接收故障
    U8 u1ClockSecondFlag         : 1;    // 时钟秒标志
    // byte 0x0E-0x0F
    Sensor_ts sPowerBoardSensor;         // 电源板传感器
    // byte 0x10
    U8  u8InverterError;                 // 变频板故障码
} LoadReadOnlyData_ts;

// 负载读写数据
typedef struct
{
    // byte 0x00
    U8 u5Reserve1                  : 5;    // 预留
    U8 u1NeutralWireMasterControl3 : 1;    // 零线总控3
    U8 u1NeutralWireMasterControl2 : 1;    // 零线总控2
    U8 u1NeutralWireMasterControl1 : 1;    // 零线总控1
    
    // byte 0x01
    U8 u1ReserveRelay              : 1;    // 预留继电器
    U8 u1MicrowaveMagnetron        : 1;    // 微波磁控管
    U8 u1SteamerInsideHeater       : 1;    // 蒸发器/盘内管
    U8 u1SteamerOutsideHeater      : 1;    // 蒸发器/盘外管
    U8 u1BackHeater                : 1;    // 背部加热管
    U8 u1BottomHeater              : 1;    // 底部加热管
    U8 u1TopInsideHeater           : 1;    // 上内加热管
    U8 u1TopOutsideHeater          : 1;    // 上外加热管
    
    // byte 0x02
    U8 u2Reserve3                  : 1;    // 预留
    U8 u1MicrowaveTurntableMotor   : 1;    // 微波转盘电机
    U8 u1MicrowaveCoolingFan       : 1;    // 微波散热风机
    U8 u1RoastingForkMotor         : 1;    // 烤叉电机
    U8 u1CoolingFan1               : 1;    // 散热风机高速     （散热风机A）
    U8 u1CoolingFan2               : 1;    // 散热风机低速/默认（散热风机B）
    U8 u2ConvectionFan2            : 1;    // 对流风机反转     （背部风机-）
    U8 u2ConvectionFan1            : 1;    // 对流风机正转/默认（背部风机+）
    
    // byte 0x03
    U8 u5Reserve4                  : 4;    // 预留
    U8 u1RestWaterBoxDrainPump     : 1;    // 余水盒排水泵（废水泵）
    U8 u1SteamerDrainValve         : 1;    // 蒸发器排水阀（废水阀）
    U8 u1SteamerDrainPump          : 1;    // 蒸发器排水泵
    U8 u1SteamerInletPump          : 1;    // 蒸发器进水泵
    
    // byte 0x04
    U8 u3Reserve5                  : 3;    // 预留
    U8 u1OxygenSensor              : 1;    // 氧传感器
    U8 u1Reserve6                  : 1;    // 预留
    U8 u1DoorLock                  : 1;    // 门锁
    U8 u1WaxMotor                  : 1;    // 蜡马达
    U8 u1OvenLamp                  : 1;    // 照明灯
    
    // byte 0x05
    U8 u7AutoDoorOpeningControl    : 7;    // 自动开门控制
    U8 u1LockAfterAutoDoorOpening  : 1;    // 自动开门锁定
    
    // byte 0x06
    U8 u8MicrowaveControl;                 // 微波控制
} LoadReadWriteData_ts;

typedef struct
{
    // byte 0x00
    U8 u6Reserve1                  : 6;     // 预留
    U8 u2LiftingPanelCtrl          : 2;     // 升降面板控制
}LoadWriteOnlyData_ts;

/* 变量定义 ------------------------------------------------------------------*/
/* 函数声明 ------------------------------------------------------------------*/
// 初始化设备
void UartComm_InitDevice(void);

// 获取设备DBA信息
UartCommDbaInfo_ts UartComm_GetDeviceStateDbaInfo(void);
UartCommDbaInfo_ts UartComm_GetDeviceControlDbaInfo(void);

// 更新负载状态，处理负载控制
void UartComm_UpdateLoadState(void);
void UartComm_HandleLoadControl(void);

// 通讯故障设备处理
void UartComm_CommErrorDeviveHandler(void);

#endif    /*** #ifndef __UARTCOMM_DEVICE_H ***/

/***************************** 文件结束 ***************************************/
