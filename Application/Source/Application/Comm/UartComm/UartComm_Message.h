/*******************************************************************************
  * 文件：UartComm_Message.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：消息
*******************************************************************************/
#ifndef __UARTCOMM_MESSAGE_H
#define __UARTCOMM_MESSAGE_H
/* 头文件 --------------------------------------------------------------------*/
#include "UartComm_Typedefine.h"
#include "UartComm_Dba.h"

/* 宏定义 --------------------------------------------------------------------*/
// 消息处理函数
#define UARTCOMM_MSG_HANDLER(fn)    \
    void fn(UartCommPort_te ePort, UartCommPacket_ts *psPacket)


/* 类型定义 ------------------------------------------------------------------*/
/* 变量定义 ------------------------------------------------------------------*/
/* 函数声明 ------------------------------------------------------------------*/
void UartComm_InitMessage(void);            // 初始化
// 发送消息处理
void UartComm_SendBroadcastProtocolInfoMsg(void);    // 发送广播协议信息
// 接收消息处理
UARTCOMM_MSG_HANDLER(UartComm_RecvReqInquireSwVerMsg);       // 接收请求查询软件版本
UARTCOMM_MSG_HANDLER(UartComm_RecvReqInquireSwIdMsg);        // 接收请求查询软件标识符
UARTCOMM_MSG_HANDLER(UartComm_RecvReqHeartbeatMsg);          // 接收请求心跳
UARTCOMM_MSG_HANDLER(UartComm_RecvReqReadDbaMsg);            // 接收请求读取DBA
UARTCOMM_MSG_HANDLER(UartComm_RecvReqWriteDbaMsg);           // 接收请求写入DBA
UARTCOMM_MSG_HANDLER(UartComm_RecvReqBitsWriteDbaMsg);       // 接收请求位写入DBA
UARTCOMM_MSG_HANDLER(UartComm_RecvReqSubscribeDbaMsg);       // 接收请求订阅DBA
UARTCOMM_MSG_HANDLER(UartComm_RecvNotifyDbaMsg);             // 接收通知DBA
UARTCOMM_MSG_HANDLER(UartComm_RecvOtaReqNotifyUpgradeMsg);   // 接收OTA请求通知升级

#endif    /*** #ifndef __UARTCOMM_MESSAGE_H ***/

/***************************** 文件结束 ***************************************/
