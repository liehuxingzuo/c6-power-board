/*******************************************************************************
  * 文件：UartComm_Driver.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：通信驱动
*******************************************************************************/
#ifndef __UARTCOMM_DRIVER_H
#define __UARTCOMM_DRIVER_H

/* 头文件 --------------------------------------------------------------------*/
#include "UartComm_Typedefine.h"
#include "UartComm_Cmd.h"

/* 宏定义 --------------------------------------------------------------------*/
/* 类型定义 ------------------------------------------------------------------*/
/* 变量定义 ------------------------------------------------------------------*/
/* 函数声明 ------------------------------------------------------------------*/
void UartComm_InitDriver(void);                                           // 初始化驱动
U8* UartComm_SetupSendPacket(U8 u8SeqNum, U8 u8DstAddr, U8 u8Command);    // 构建发送数据包
void UartComm_SendPacket(UartCommPort_te ePort, U8 *pu8DataEnd);          // 发送数据包
void UartComm_SendByteHandler(UartCommPort_te ePort);                     // 发送字节处理
void UartComm_RecvByteHandler(UartCommPort_te ePort);                     // 接收字节处理

#endif    /*** #ifndef __UARTCOMM_DRIVER_H ***/

/***************************** 文件结束 ***************************************/
