/*******************************************************************************
  * 文件：UartComm_DispatchMsg.c
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：派遣消息
*******************************************************************************/
/* 头文件 *********************************************************************/
#include "UartComm_DispatchMsg.h"
#include "UartComm_Cmd.h"
#include "UartComm_Message.h"
#include "Macro.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量定义 *******************************************************************/
// 消息处理表
static const UartCommMsgHandler_ts asMsgHandlerTab[] =
{
    // 通知DBA
    {
        u8UARTCOMM_CMD_NOTIFY_DBA,
        UartComm_RecvNotifyDbaMsg
    },
    // 请求读取DBA
    {
        u8UARTCOMM_CMD_REQ_READ_DBA,
        UartComm_RecvReqReadDbaMsg
    },
    // 请求写入DBA
    {
        u8UARTCOMM_CMD_REQ_WRITE_DBA,
        UartComm_RecvReqWriteDbaMsg
    },
    // 请求位写入DBA
    {
        u8UARTCOMM_CMD_REQ_BITS_WRITE_DBA,
        UartComm_RecvReqBitsWriteDbaMsg
    },
    // 请求订阅DBA
    {
        u8UARTCOMM_CMD_REQ_SUBSCRIBE_DBA,
        UartComm_RecvReqSubscribeDbaMsg
    },
    // 请求查询软件版本
    {
        u8UARTCOMM_CMD_REQ_INQUIRE_SW_VER,
        UartComm_RecvReqInquireSwVerMsg
    },
    // 请求查询软件标识符
    {
        u8UARTCOMM_CMD_REQ_INQUIRE_SW_ID,
        UartComm_RecvReqInquireSwIdMsg
    },
    // 请求心跳
    {
        u8UARTCOMM_CMD_REQ_HEARTBEAT,
        UartComm_RecvReqHeartbeatMsg
    },
    // OTA请求通知升级
    {
        u8UARTCOMM_CMD_OTA_REQ_NOTIFY_UPGRADE,
        UartComm_RecvOtaReqNotifyUpgradeMsg
    },
};

// 路由信息表
static const UartCommRoutingInfo_ts asRoutingInfoTab[] =
{
    { u8UARTCOMM_TEST_TOOL_ADDR, ePORT_DISPLAY_BOARD },
};

/* 函数声明 *******************************************************************/
/* 函数定义 *******************************************************************/

/*******************************************************************************
  * 函数名：UartComm_LookupPort
  * 功  能：查找端口
  * 参  数：u8DesAddr - 目的地址
  *         pePort    - 端口指针
  * 返回值：查找结果
  * 说  明：无
*******************************************************************************/
Bool UartComm_LookupPort(U8 u8DesAddr, UartCommPort_te *pePort)
{
    U8 u8Index;
    Bool bResult = FALSE;

    // 获取路由信息表大小
    static U8 u8TabSize = ARRAY_SIZE(asRoutingInfoTab);

    // 遍历处理表
    for(u8Index=0; u8Index < u8TabSize; u8Index++)
    {
        // 找到地址
        if(asRoutingInfoTab[u8Index].u8Address == u8DesAddr)
        {
            // 设置结果，获取端口
            bResult = TRUE;
            *pePort = asRoutingInfoTab[u8Index].ePort;
            break;
        }
    }

    // 返回结果
    return bResult;
}

/*******************************************************************************
  * 函数名: UartComm_DispatchMsg
  * 功  能：派遣消息
  * 参  数：ePort    - 端口
  *         psPacket - 数据包指针
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void UartComm_DispatchMsg(UartCommPort_te ePort, UartCommPacket_ts *psPacket)
{
    U8 u8Index;

    // 获取命令处理表大小
    static U8 u8TabSize = ARRAY_SIZE(asMsgHandlerTab);

    // 遍历处理表
    for(u8Index=0; u8Index < u8TabSize; u8Index++)
    {
        // 找到处理函数
        if(asMsgHandlerTab[u8Index].u8Command == psPacket->u8Command)
        {
            // 执行处理函数
            if(asMsgHandlerTab[u8Index].pfHandler != (void*)NULL)
            {
                asMsgHandlerTab[u8Index].pfHandler(ePort, psPacket);
            }
            break;
        }
    }
}

/***************************** 文件结束 ***************************************/
