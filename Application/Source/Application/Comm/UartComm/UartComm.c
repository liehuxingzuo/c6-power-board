/*******************************************************************************
  * 文件：UartComm.c
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：串口通信
*******************************************************************************/
/* 头文件 *********************************************************************/
#include "UartComm.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量定义 *******************************************************************/
static Bool bCommError = FALSE;     // 通信故障标志
static Timer_ts sCommErrorTimer;    // 通信故障定时器
static Timer_ts sSendMsgTimer;      // 发送消息定时器

/* 函数声明 *******************************************************************/
static void UartComm_CommReady(void);                 // 通信准备好
static void UartComm_CommErrorTimerCallback(void);    // 通信故障定时器回调函数

/* 函数定义 *******************************************************************/
/*******************************************************************************
  * 函数名: UartComm_Init
  * 功  能：初始化
  * 参  数：无
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void UartComm_Init(void)
{
    // 初始化
    UartComm_InitDriver();
    UartComm_InitMessage();

    // 创建任务，更新负载状态
    OS_TaskCreate(UartComm_UpdateLoadState);

    // 开启发送消息定时器
    OS_TimerStart(&sSendMsgTimer, 1000, UartComm_CommReady);
}

/*******************************************************************************
  * 函数名: UartComm_CommReady
  * 功  能：通信准备好
  * 参  数：无
  * 返回值：无
  * 说  明：无
*******************************************************************************/
static void UartComm_CommReady(void)
{
    // 停止发送消息定时器
    OS_TimerStop(&sSendMsgTimer);

    // 发送广播协议信息消息
    UartComm_SendBroadcastProtocolInfoMsg();

    // 开启通信故障定时器
    OS_TimerStart(&sCommErrorTimer, u16COMM_ERROR_TIME,
                  UartComm_CommErrorTimerCallback);

    // 发布DBA数据
    UartComm_PublishDbaData(ePORT_DISPLAY_BOARD, TRUE);
}

/*******************************************************************************
  * 函数名: UartComm_CommErrorTimerCallback
  * 功  能：通信故障定时器回调函数
  * 参  数：无
  * 返回值：无
  * 说  明：无
*******************************************************************************/
static void UartComm_CommErrorTimerCallback(void)
{
    // 设置通信故障标志
    bCommError = TRUE;
    // 通信故障处理
    UartComm_CommErrorDeviveHandler();
}

/*******************************************************************************
  * 函数名: UartComm_ClearCommError
  * 功  能：清除通信故障
  * 参  数：无
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void UartComm_ClearCommError(void)
{
    // 清除通信故障标志
    bCommError = FALSE;
    // 重置通信故障定时器
    OS_TimerReset(&sCommErrorTimer);
}

/*******************************************************************************
  * 函数名: UartComm_GetCommErrorState
  * 功  能：获取通信故障状态
  * 参  数：无
  * 返回值：无
  * 说  明：无
*******************************************************************************/
Bool UartComm_GetCommErrorState(void)
{
    // 返回通信故障标志
    return bCommError;
}

/***************************** 文件结束 ***************************************/
