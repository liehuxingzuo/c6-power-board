/*******************************************************************************
  * 文件：UartComm_Dba.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：数据块阵列
*******************************************************************************/
#ifndef __UARTCOMM_DBA_H
#define __UARTCOMM_DBA_H
/* 头文件 --------------------------------------------------------------------*/
#include "UartComm_Typedefine.h"
#include "UartComm_Device.h"

/* 宏定义 --------------------------------------------------------------------*/
/* 类型定义 ------------------------------------------------------------------*/
/* 变量定义 ------------------------------------------------------------------*/
/* 函数声明 ------------------------------------------------------------------*/
// 初始化
void UartComm_InitDba(void);

// 读写设备状态数据块
U8 UartComm_ReadWriteDeviceStateDataBlock(
    UartCommDataReadWrite_te eDataReadWrite,
    U16 u16DataAddr, U8 u8DataLen, U8 *pu8Data);

// 读写设备控制数据块
U8 UartComm_ReadWriteDeviceControlDataBlock(
    UartCommDataReadWrite_te eDataReadWrite,
    U16 u16DataAddr, U8 u8DataLen, U8 *pu8Data);

// 设置订阅DBA参数
Bool UartComm_SetSubscribeDbaParm(
    UartCommPort_te ePort, U8 u8Address,
    U8 u8Time, U8 u8DataBlockNum, U8 *pu8DataBlockParm);

// 发布DBA数据
void UartComm_PublishDbaData(UartCommPort_te ePort, Bool bState);

#endif    /*** #ifndef __UARTCOMM_DBA_H ***/

/***************************** 文件结束 ***************************************/
