/*******************************************************************************
  * 文件：BoardCommC7_Dispatch.c
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2019-05-15
  * 说明: 显示板与电源板C7通信协议，C7协议为海尔内部协议
*******************************************************************************/
#include "BoardCommC7_Dispatch.h"
#include "BoardCommC7_Handler.h"

// 报文处理派遣数组
const static FrameHandler_ts asFrameHandler[] = 
{
    // 显示板/流程板下行报文处理
    {u16BOARD_COMM_C7_DOWNWARD, FrameDownwardHandler}
};

/*******************************************************************************
  * 函数名: BoardCommRecvFrameTask
  * 功  能  ：底板通讯接收报文解析任务
  * 参  数 ：无
  * 返回值：U8 :报文长度
  * 说  明 ：无
*******************************************************************************/
U8 BoardCommFrame_Dispatch(BoardFrame_ts* psFrame)
{
    U8 u8Index;
    
    // 获取报文帧类型
    U16 u16Type = (((U16)psFrame->u8Cmd_H) << 8) + (psFrame->u8Cmd_L);

    // 报文处理派遣
    for(u8Index = 0;u8Index < ARRAY_SIZE(asFrameHandler);u8Index++)
    { 
        if(u16Type == asFrameHandler[u8Index].u16Type)
        {
            asFrameHandler[u8Index].Function(psFrame->au8Data,psFrame->u8Len - 9);
            break;
        }
    }

    // 返回报文长度
    return psFrame->u8Len + 3;
}

