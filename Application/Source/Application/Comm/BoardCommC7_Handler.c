/*******************************************************************************
  * 文件：BoardCommC7_Handler.c
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2019-05-15
  * 说明: 显示板与电源板C7通信协议，C7协议为海尔内部协议
*******************************************************************************/
#include "BoardCommC7_Handler.h"
#include "RelayCtrl.h"
#include "RelayPower.h"
#include "MotorCtrl.h"
#include "OxygenSensor.h"
#include "Hardware_IO.h"
#include "ComFun.h"
#include "Debug.h"
#include <string.h>

/*******************************************************************************
  * 函数名: FrameDownwardHandler
  * 功  能  ：显示板/流程板下行报文处理
  * 参  数 ：U8 au8Data[]：下行报文数据域，即负载控制信息
  *         U8 u8Len：数据域长度
  * 返回值：无
  * 说  明 ：无
*******************************************************************************/
void FrameDownwardHandler(const U8 au8Data[],U8 u8Len)
{
    if(u8Len != 13)
    {
    	// 数据域长度非法，提前退出
    	return;
    }

	// 用于监测接收到的数据
//	BoardCommRecvData_ts sRecvData;
//	memcpy(&sRecvData,au8Data,u8Len);

//	DEBUG("Recv data:");
//	DEBUG_ARR(au8Data, u8Len);
	BoardCommRecvData_ts *psRecvData = (BoardCommRecvData_ts *)au8Data;

	// 上外加热管处理
	if(ON == psRecvData->bUpOutHeatTubeCtrl && OFF == RelayGetUpOutHeatTubeState())
	{
		// 开启上外加热管
		RelayCtrlUpOutHeatTube(ON);
	}
	else if(OFF == psRecvData->bUpOutHeatTubeCtrl && ON == RelayGetUpOutHeatTubeState())
	{
		// 关闭上外加热管
		RelayCtrlUpOutHeatTube(OFF);
	}

    // 上内加热管处理
   	if(ON == psRecvData->bUpInHeatTubeCtrl && OFF == RelayGetUpInHeatTubeState())
	{
		// 开启上内加热管
		RelayCtrlUpInHeatTube(ON);
	}
	else if(OFF == psRecvData->bUpInHeatTubeCtrl && ON == RelayGetUpInHeatTubeState())
	{
		// 关闭上内加热管
		RelayCtrlUpInHeatTube(OFF);
	}

	// 下加热管处理
   	if(ON == psRecvData->bDownHeatTubeCtrl && OFF == RelayGetDownHeatTubeState())
	{
		// 开启下加热管
		RelayCtrlDownHeatTube(ON);
	}
	else if(OFF == psRecvData->bDownHeatTubeCtrl && ON == RelayGetDownHeatTubeState())
	{
		// 关闭下加热管
		RelayCtrlDownHeatTube(OFF);
	}

	// 后热管处理
   	if(ON == psRecvData->bBackHeatTubeCtrl && OFF == RelayGetBackHeatTubeState())
	{
		// 开启后加热管
		RelayCtrlBackHeatTube(ON);
	}
	else if(OFF == psRecvData->bBackHeatTubeCtrl && ON == RelayGetBackHeatTubeState())
	{
		// 关闭后加热管
		RelayCtrlBackHeatTube(OFF);
	}

	// 蒸发盘外处理
   	if(ON == psRecvData->bOutsideSteamPanCtrl && OFF == RelayGetSteamPanOutState())
	{
		// 开启蒸发盘外管
		RelayCtrlSteamPanOut(ON);
	}
	else if(OFF == psRecvData->bOutsideSteamPanCtrl && ON == RelayGetSteamPanOutState())
	{
		// 关闭蒸发盘外管
		RelayCtrlSteamPanOut(OFF);
	}

	// 蒸发盘内处理
   	if(ON == psRecvData->bInsideSteamPanCtrl && OFF == RelayGetSteamPanInState())
	{
		// 开启蒸发盘内管
		RelayCtrlSteamPanIn(ON);
	}
	else if(OFF == psRecvData->bInsideSteamPanCtrl && ON == RelayGetSteamPanInState())
	{
		// 关闭蒸发盘内管
		RelayCtrlSteamPanIn(OFF);
	}

	// 背部风扇+处理
   	if(ON == psRecvData->bBackFan1Ctrl && OFF == RelayGetBackFan1State())
	{
		// 开启背部风扇+
		RelayCtrlBackFan1(ON);
	}
	else if(OFF == psRecvData->bBackFan1Ctrl && ON == RelayGetBackFan1State())
	{
		// 关闭背部风扇+
		RelayCtrlBackFan1(OFF);
	}
	
	// 背部风扇-处理
   	if(ON == psRecvData->bBackFan2Ctrl && OFF == RelayGetBackFan2State())
	{
		// 开启背部风扇-
		RelayCtrlBackFan2(ON);
	}
	else if(OFF == psRecvData->bBackFan2Ctrl && ON == RelayGetBackFan2State())
	{
		// 关闭背部风扇-
		RelayCtrlBackFan2(OFF);
	}

	// 散热风扇A处理
   	if(ON == psRecvData->bCoolFan1Ctrl && OFF == RelayGetCoolFan1State())
	{
		// 开启散热风扇A
		RelayCtrlCoolFan1(ON);
	}
	else if(OFF == psRecvData->bCoolFan1Ctrl && ON == RelayGetCoolFan1State())
	{
		// 关闭散热风扇A
		RelayCtrlCoolFan1(OFF);
	}

	// 散热风扇B处理
   	if(ON == psRecvData->bCoolFan2Ctrl && OFF == RelayGetCoolFan2State())
	{
		// 开启散热风扇A
		RelayCtrlCoolFan2(ON);
	}
	else if(OFF == psRecvData->bCoolFan2Ctrl && ON == RelayGetCoolFan2State())
	{
		// 关闭散热风扇A
		RelayCtrlCoolFan2(OFF);
	}

	// 炉灯处理
   	if(ON == psRecvData->bLampCtrl && OFF == RelayGetLampState())
	{
		// 开启炉灯处理
		RelayCtrlLamp(ON);
	}
	else if(OFF == psRecvData->bLampCtrl && ON == RelayGetLampState())
	{
		// 关闭炉灯处理
		RelayCtrlLamp(OFF);
	}

	// 蜡马达处理
   	if(ON == psRecvData->bWaxMotorCtrl && OFF == RelayGetMotorState())
	{
		// 开启蜡马达
		RelayCtrlMotor(ON);
	}
	else if(OFF == psRecvData->bWaxMotorCtrl && ON == RelayGetMotorState())
	{
		// 关闭蜡马达
		RelayCtrlMotor(OFF);
	}

	// 预留继电器处理
   	if(ON == psRecvData->bReserveCtrl && OFF == RelayGetReserveState())
	{
		// 开启预留继电器
		RelayCtrlReserve(ON);
	}
	else if(OFF == psRecvData->bReserveCtrl && ON == RelayGetReserveState())
	{
		// 关闭预留继电器
		RelayCtrlReserve(OFF);
	}

	// 零总1继电器控制
    RelayZeroLine1Ctrl(psRecvData->bZeroLine1Ctrl);

    // 零总2继电器控制
    RelayZeroLine2Ctrl(psRecvData->bZeroLine2Ctrl);

    // 出水阀控制
    Hardware_OutletValveCtrl(psRecvData->bDischargePumpCtrl);

    // 进水阀控制
    Hardware_InletValveCtrl(psRecvData->bIntakePumpCtrl);

	// 翻转电机控制	
	if(psRecvData->bLiftingMotorCtrl == 1)
	{
		// 面板上升
		Motor_Uprise();
	}
	else if(psRecvData->bLiftingMotorCtrl == 2)
	{
		// 面板下降
		Motor_Downgrade();
	}
	
    // 氧传感器控制
    if(psRecvData->bOxygenSensorCtrl)
    {
    	// 检测氧气传感器是否启动工作
    	if(FALSE == OxygenSensorIsWork())
    	{
    		// 氧气传感器开始工作
    		OxygenSensor_Start();
    	}
    }
    else
    {
    	// 检测氧气传感器是否启动工作
    	if(TRUE == OxygenSensorIsWork())
    	{
    		// 氧气传感器停止工作
    		OxygenSensor_Stop();
    	}
    }
}

