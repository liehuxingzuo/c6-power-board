/*******************************************************************************
  * 文件：BoardCommC7_Dispatch.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2019-05-15
  * 说明: 显示板与电源板C7通信协议，C7协议为海尔内部协议
*******************************************************************************/
#ifndef _BOARD_COMM_C7_DISPATCH_H
#define _BOARD_COMM_C7_DISPATCH_H

// 通用头文件
#include "Typedefine.h"
#include "Constant.h"
#include "Macro.h"  
#include "BoardCommC7_Comm.h"

typedef struct
{
    U16   u16Type;                                  // 帧类型
    void (*Function)(const U8 au8Data[],U8 u8Len); // 处理函数指针
}FrameHandler_ts;// 报文处理

U8 BoardCommFrame_Dispatch(BoardFrame_ts* psFrame);	// 合法报文派遣处理

#endif
