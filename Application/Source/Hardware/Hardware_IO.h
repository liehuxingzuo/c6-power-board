/*******************************************************************************
  * 文件：Hardware_IO.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-04
  * 说明：IO相关
*******************************************************************************/
#ifndef __HARDWARE_IO_H
#define __HARDWARE_IO_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
void Hardware_InitIO(void);					            // IO初始化

/***输出控制***/
void Hardware_SteamPanOutCtrl(Bool bState);             // 蒸发盘外控制
void Hardware_SteamPanInCtrl(Bool bState);              // 蒸发盘内控制
void Hardware_ZeroLine1Ctrl(Bool bState);               // 零总1控制
void Hardware_ZeroLine2Ctrl(Bool bState);               // 零总2控制
void Hardware_CoolFanACtrl(Bool bState);                // 冷却风扇A控制
void Hardware_CoolFanBCtrl(Bool bState);                // 冷却风扇B控制
void Hardware_WaxMotorCtrl(Bool bState);                // 蜡马达控制
void Hardware_BackFan1Ctrl(Bool bState);                // 背部风扇+控制
void Hardware_BackFan2Ctrl(Bool bState);                // 背部风扇-控制
void Hardware_ReserveCtrl(Bool bState);                 // 预留继电器控制
void Hardware_LampCtrl(Bool bState);                    // 照明灯控制
void Hardware_BackHeatCtrl(Bool bState);                // 背部加热控制
void Hardware_UpOutHeatCtrl(Bool bState);               // 上外加热控制
void Hardware_DownHeatCtrl(Bool bState);                // 下加热控制
void Hardware_UpInHeatCtrl(Bool bState);                // 上内加热控制
void Hardware_MTIN1Ctrl(Bool bState);                   // 翻转电机MT_IN1控制
void Hardware_MTIN2Ctrl(Bool bState);                   // 翻转电机MT_IN2控制
void Hardware_Oxygen5VolCtrl(Bool bState);              // 氧气传感器5V控制
void Hardware_Oxygen12VolCtrl(Bool bState);             // 氧气传感器12V控制
void Hardware_12VolCtrl(Bool bState);                   // 12V控制
void Hardware_NTCSelxCtrl(Bool bState);                 // NTC分压电阻Sel_x控制
void Hardware_OutletValveCtrl(Bool bState);             // 出水阀控制
void Hardware_InletValveCtrl(Bool bState);              // 进水阀控制
void Hardware_WaterPosPowerCtrl(Bool bState);           // 水位计电源控制
void Hardware_EffluentValveCtrl(Bool bState);           // 废水阀控制
void Hardware_EffluentPumpCtrl(Bool bState);            // 废水泵控制

/***输入检测***/
Bool Hardware_GetInverterOSC(void);                     // 获取变频器OSC电平
Bool Hardware_GetEffluentValveState(void);              // 获取废水阀控制状态
Bool Hardware_GetEffluentPumpState(void);               // 获取废水泵控制状态
Bool Hardware_GetOutletValveState(void);                // 获取出水阀控制状态
Bool Hardware_GetInletValveState(void);                 // 获取进水阀控制状态
Bool Hardware_OpenStopCheck(void);                      // 面板打开限位检测
Bool Hardware_CloseStopCheck(void);                     // 面板关闭限位检测
Bool Hardware_DoorStateCheck(void);                     // 门开关检测
Bool Hardware_WasteWaterBoxCheck(void);                 // 废水盒位置检测
Bool Hardware_WasteWaterFullCheck(void);                // 废水盒满检测
Bool Hardware_SteamWaterPosCheck(void);                 // 蒸发器水位计检测
Bool Hardware_LeftWaterPosCheck(void);                  // 余水盒水位检测
Bool Hardware_CleanWaterLackCheck(void);                // 清水盒缺水检测
Bool Hardware_SelxCheck(void);                          // Selx检测
Bool Hardware_ProbTypeCheck(void);                      // 食物探针类型判断
Bool Hardware_ZeroCrossPort(void);                      // 过零检测端口
Bool Hardware_GetInverterOSC(void);                     // 获取变频器OSC电平

#endif /* __HARDWARE_IO_H */

/***************************** 文件结束 ***************************************/
