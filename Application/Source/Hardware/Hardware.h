/*******************************************************************************
  * 文件：Hardware.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：硬件相关
*******************************************************************************/
#ifndef __HARDWARE_H
#define __HARDWARE_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"
//#include "Hardware_System.h"
//#include "Hardware_WDT.h"
//#include "Hardware_IO.h"
//#include "Hardware_Timer.h"
//#include "Hardware_Uart.h"
//#include "Hardware_ADC.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/
void Hardware_Init(void);                // 初始化
void Hardware_DisableInterrupt(void);    // 禁止中断
void Hardware_EnableInterrupt(void);     // 使能中断
void Hardware_EnterCritical(void);       // 进入临界区
void Hardware_ExitCritical(void);        // 退出临界区
void Hardware_DelayUs(U32 u32Us);        // 延时微秒

#endif /* __HARDWARE_H */

/***************************** 文件结束 ***************************************/
