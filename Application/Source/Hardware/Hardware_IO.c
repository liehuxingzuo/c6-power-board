/*******************************************************************************
  * 文件：Hardware_IO.h
  * 作者：djy
  * 版本：v1.0.0
  * 日期：2021-02-04
  * 说明：IO相关
*******************************************************************************/

/* 头文件 *********************************************************************/
#include "Hardware_IO.h"
#include "r_cg_macrodriver.h"
#include "Hardware.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量定义 *******************************************************************/
/* 函数声明 *******************************************************************/
/* 函数定义 *******************************************************************/

/*******************************************************************************
  * 函数名：Hardware_InitIO
  * 功  能：初始化
  * 参  数：无
  * 返回值：无
  * 说  明：无
*******************************************************************************/
void Hardware_InitIO(void)
{

}

// 蒸发盘外控制
void Hardware_SteamPanOutCtrl(Bool bState)
{
    if(bState)
    {
        P0_bit.no5 = 1;
    }
    else
    {
        P0_bit.no5 = 0;
    }
}

// 蒸发盘内控制
void Hardware_SteamPanInCtrl(Bool bState)
{
    if(bState)
    {
        P3_bit.no0 = 1;
    }
    else
    {
        P3_bit.no0 = 0;
    }
}

// 零总1控制
void Hardware_ZeroLine1Ctrl(Bool bState)
{
    if(bState)
    {
        P0_bit.no6 = 1;
    }
    else
    {
        P0_bit.no6 = 0;
    }
}

// 零总2控制
void Hardware_ZeroLine2Ctrl(Bool bState)
{
    if(bState)
    {
        P5_bit.no4 = 1;
    }
    else
    {
        P5_bit.no4 = 0;
    }
}

// 冷却风扇A控制
void Hardware_CoolFanACtrl(Bool bState)
{
    if(bState)
    {
        P7_bit.no0 = 1;
    }
    else
    {
        P7_bit.no0 = 0;
    }
}

// 冷却风扇B控制
void Hardware_CoolFanBCtrl(Bool bState)
{
    if(bState)
    {
        P7_bit.no1 = 1;
    }
    else
    {
        P7_bit.no1 = 0;
    }
}

// 蜡马达控制
void Hardware_WaxMotorCtrl(Bool bState)
{
    if(bState)
    {
        P7_bit.no2 = 1;
    }
    else
    {
        P7_bit.no2 = 0;
    }
}

// 背部风扇+控制
void Hardware_BackFan1Ctrl(Bool bState)
{
    if(bState)
    {
        P7_bit.no3 = 1;
    }
    else
    {
        P7_bit.no3 = 0;
    }
}

// 背部风扇-控制
void Hardware_BackFan2Ctrl(Bool bState)
{
    if(bState)
    {
        P7_bit.no4 = 1;
    }
    else
    {
        P7_bit.no4 = 0;
    }
}

// 预留继电器控制
void Hardware_ReserveCtrl(Bool bState)
{
    if(bState)
    {
        P7_bit.no5 = 1;
    }
    else
    {
        P7_bit.no5 = 0;
    }
}

// 照明灯控制
void Hardware_LampCtrl(Bool bState)
{
    if(bState)
    {
        P5_bit.no5 = 1;
    }
    else
    {
        P5_bit.no5 = 0;
    }
}

// 背部加热控制
void Hardware_BackHeatCtrl(Bool bState)
{
    if(bState)
    {
        P5_bit.no3 = 1;
    }
    else
    {
        P5_bit.no3 = 0;
    }
}

// 上外加热控制
void Hardware_UpOutHeatCtrl(Bool bState)
{
    if(bState)
    {
        P5_bit.no2 = 1;
    }
    else
    {
        P5_bit.no2 = 0;
    }
}

// 下加热控制
void Hardware_DownHeatCtrl(Bool bState)
{
    if(bState)
    {
        P5_bit.no1 = 1;
    }
    else
    {
        P5_bit.no1 = 0;
    }
}

// 上内加热控制
void Hardware_UpInHeatCtrl(Bool bState)
{
    if(bState)
    {
        P5_bit.no0 = 1;
    }
    else
    {
        P5_bit.no0 = 0;
    }
}

// 翻转电机MT_IN1控制
void Hardware_MTIN1Ctrl(Bool bState)
{
    if(bState)
    {
        P0_bit.no0 = 1;
    }
    else
    {
        P0_bit.no0 = 0;
    }
}

// 翻转电机MT_IN2控制
void Hardware_MTIN2Ctrl(Bool bState)
{
    if(bState)
    {
        P14_bit.no1 = 1;
    }
    else
    {
        P14_bit.no1 = 0;
    }
}

// 氧气传感器5V控制
void Hardware_Oxygen5VolCtrl(Bool bState)
{
    if(bState)
    {
        P13_bit.no0 = 1;
    }
    else
    {
        P13_bit.no0 = 0;
    }
}

// 氧气传感器12V控制
void Hardware_Oxygen12VolCtrl(Bool bState)
{
    if(bState)
    {
        P1_bit.no5 = 1;
    }
    else
    {
        P1_bit.no5 = 0;
    }
}

// 12V控制
void Hardware_12VolCtrl(Bool bState)
{
    if(bState)
    {
        P7_bit.no6 = 1;
    }
    else
    {
        P7_bit.no6 = 0;
    }
}

// NTC分压电阻Sel_x控制
void Hardware_NTCSelxCtrl(Bool bState)
{
    // 配置为输出模式
    PM14_bit.no7 = 0;

    if(bState)
    {
        P14_bit.no7 = 1;
    }
    else
    {
        P14_bit.no7 = 0;
    }
}

// 出水阀控制
void Hardware_OutletValveCtrl(Bool bState)
{
    if(bState)
    {
        P14_bit.no6 = 1;
    }
    else
    {
        P14_bit.no6 = 0;
    }
}

// 进水阀控制
void Hardware_InletValveCtrl(Bool bState)
{
    if(bState)
    {
        P1_bit.no0 = 1;
    }
    else
    {
        P1_bit.no0 = 0;
    }
}

// 水位计电源控制
void Hardware_WaterPosPowerCtrl(Bool bState)
{
    if(bState)
    {
        P2_bit.no7 = 1;
    }
    else
    {
        P2_bit.no7 = 0;
    }
}

// 废水阀控制
void Hardware_EffluentValveCtrl(Bool bState)
{
    if(bState)
    {
        P1_bit.no3 = 1;
    }
    else
    {
        P1_bit.no3 = 0;
    }
}

// 废水泵控制
void Hardware_EffluentPumpCtrl(Bool bState)
{
    if(bState)
    {
        P1_bit.no4 = 1;
    }
    else
    {
        P1_bit.no4 = 0;
    }
}

// 获取变频器OSC电平
Bool Hardware_GetInverterOSC(void)
{
    return P4_bit.no2;
}

// 获取废水阀控制状态
Bool Hardware_GetEffluentValveState(void)
{
    return P1_bit.no3;
}

// 获取废水泵控制状态
Bool Hardware_GetEffluentPumpState(void)
{
    return P1_bit.no4;
}

// 获取出水阀控制状态
Bool Hardware_GetOutletValveState(void)
{
	return P14_bit.no6;
}

// 获取进水阀控制状态
Bool Hardware_GetInletValveState(void)
{
	return P1_bit.no0;
}

// 面板打开限位检测，常开开关（上拉至高电平），到位后闭合（接地）
Bool Hardware_OpenStopCheck(void)
{
    return P4_bit.no3;
}

// 面板关闭限位检测，常开开关（上拉至高电平），到位后闭合（接地）
Bool Hardware_CloseStopCheck(void)
{
    return P12_bit.no0;
}

// 门开关检测
Bool Hardware_DoorStateCheck(void)
{
    return P14_bit.no0;
}

// 废水盒位置检测
Bool Hardware_WasteWaterBoxCheck(void)
{
    return P0_bit.no1;
}

// 废水盒满检测
Bool Hardware_WasteWaterFullCheck(void)
{
    return P0_bit.no4;
}

// 水位计检测（蒸发器）
Bool Hardware_SteamWaterPosCheck(void)
{
    return P2_bit.no6;
}

// 水位传感器检测（余水盒水位）
Bool Hardware_LeftWaterPosCheck(void)
{
    return P14_bit.no1;
}

// 清水盒缺水检测
Bool Hardware_CleanWaterLackCheck(void)
{
    return P0_bit.no0;
}

// Selx检测
Bool Hardware_SelxCheck(void)
{
    // 配置为输入模式
    PM14_bit.no7 = 1;

    return P14_bit.no7;
}

// 食物探针类型判断
Bool Hardware_ProbTypeCheck(void)
{
    // 配置为输入模式
    PM6_bit.no3 = 1;

    return P6_bit.no3;
}

// 过零检测端口
Bool Hardware_ZeroCrossPort(void)
{
    return P13_bit.no7;
}

/***************************** 文件结束 ***************************************/
