/*******************************************************************************
  * 文件：ImageHeader.h
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：Image Header
*******************************************************************************/
#ifndef __IMAGE_HEADER_H
#define __IMAGE_HEADER_H

/* 头文件 *********************************************************************/
#include "Typedefine.h"
#include "Constant.h"

/* 宏定义 *********************************************************************/
// 头信息
#define u32BOOTLOADER_HEADER_ADDRESS     ((U32) 0x00100)    // 引导程序头地址
#define u32APPLICATION_HEADER_ADDRESS    ((U32) 0x03000)    // 应用程序头地址
// 程序信息
#define u32IMAGE_ADDRESS    ((U32) 0x03000)    // 程序地址
#define u32IMAGE_SIZE       ((U32) 0x0D000)    // 程序大小
#define u32PARM_ADDRESS     ((U32) 0x10000)    // 参数地址
#define u32PARM_SIZE        ((U32) 0x00000)    // 参数大小

/* 类型定义 *******************************************************************/
// 程序类型
typedef enum
{
    eIMAGE_TYPE_BOOTLOADER   = 0x00,    // 引导程序
    eIMAGE_TYPE_APPLICATION  = 0x01,    // 应用程序
    //eIMAGE_TYPE_STAGING_AREA = 0x02     // 暂存区
} ImageType_te;

// 程序头信息
#pragma pack(push, 1)
typedef struct
{
    U8 u8ImageType;           // 程序类型
    U8 u8MajorVersion;        // 主版本
    U8 u8MinorVersion;        // 次版本
    U8 u8RevisionVersion;     // 修订版本
    U16 u16ReleaseYear;       // 发布年
    U8 u8ReleaseMonth;        // 发布月
    U8 u8ReleaseDay;          // 发布日
    U8 au8ImageId[20];        // 标识符
    U32 u32ImageAddress;      // 程序地址
    U32 u32ImageSize;         // 程序大小
    U32 u32ParmAddress;       // 参数地址
    U32 u32ParmSize;          // 参数大小
    U32 u32StartupAddress;    // 启动地址
    U16 u16ImageDataCRC;      // 数据校验
    U16 u16ImageHeaderCRC;    // 头信息校验
} __far ImageHeader_ts;
#pragma pack(pop)

// 关于标识符的说明：
// 标识符为20字节，不足的补'\0';
// 标识符编码规则："专用号_编号"
// "专用号"为板子专用号（PLM系统申请）
// "编号"占2个字符，用于区分同一个板子上的不同芯片
// "编号"按照"00","01","02"...顺序编号
// 比如专用号为"0123456789AB"的板子上的芯片标识符为
// "0123456789AB_00","0123456789AB_01","0123456789AB_02"...
// 通常大部分板子上只有一个芯片，这种情况可省略"_编号"部分
// 即只写专用号，如"0123456789AB"

/* 变量声明 *******************************************************************/
/* 函数声明 *******************************************************************/


#endif /* __IMAGE_HEADER_H */

/***************************** 文件结束 ***************************************/
