/*******************************************************************************
  * 文件：InterruptVectorTable.c
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：中断向量表
*******************************************************************************/
/* 头文件 *********************************************************************/
#include "JumpFunction.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量定义 *******************************************************************/
void _iar_program_start(void);
__interrupt void r_intc0_interrupt(void);
__interrupt void r_uart0_interrupt_receive(void);
__interrupt void r_uart0_interrupt_send(void);
__interrupt void r_uart1_interrupt_receive(void);
__interrupt void r_uart1_interrupt_send(void);
__interrupt void r_tau0_channel3_interrupt(void);
__interrupt void r_tau0_channel4_interrupt(void);
__interrupt void r_tau0_channel5_interrupt(void);

// 中断向量表
__root const InterruptRoutine_tpf apfInteruptVectorTable[] =
{
	(InterruptRoutine_tpf)_iar_program_start,          // RESET    Vect_00
	(InterruptRoutine_tpf)NULL,                        // DEBUG    Vect_02
	(InterruptRoutine_tpf)NULL,                        // INTWDTI  Vect_04
	(InterruptRoutine_tpf)NULL,                        // INTLVI   Vect_06
	(InterruptRoutine_tpf)r_intc0_interrupt,           // INTP0    Vect_08
	(InterruptRoutine_tpf)NULL,                        // INTP1    Vect_0A
	(InterruptRoutine_tpf)NULL,                        // INTP2    Vect_0C
	(InterruptRoutine_tpf)NULL,                        // INTP3    Vect_0E
	(InterruptRoutine_tpf)NULL,                        // INTP4    Vect_10
	(InterruptRoutine_tpf)NULL,                        // INTP5    Vect_12
	(InterruptRoutine_tpf)NULL,                        // INTST2   Vect_14
	(InterruptRoutine_tpf)NULL,                        // INTSR2   Vect_16
	(InterruptRoutine_tpf)NULL,                        // INTSRE2  Vect_18
	(InterruptRoutine_tpf)NULL,                        // NTDMA0   Vect_1A
	(InterruptRoutine_tpf)NULL,                        // NTDMA1   Vect_1C
	(InterruptRoutine_tpf)r_uart0_interrupt_send,      // INTST0   Vect_1E
	(InterruptRoutine_tpf)r_uart0_interrupt_receive,   // INTSR0   Vect_20
	(InterruptRoutine_tpf)NULL,                        // INTSRE0  Vect_22
	(InterruptRoutine_tpf)r_uart1_interrupt_send,      // INTST1   Vect_24
	(InterruptRoutine_tpf)r_uart1_interrupt_receive,   // INTSR1   Vect_26
	(InterruptRoutine_tpf)NULL,                        // INTSRE1  Vect_28
	(InterruptRoutine_tpf)NULL,                        // INTIICA0 Vect_2A
	(InterruptRoutine_tpf)NULL,                        // INTTM00  Vect_2C
	(InterruptRoutine_tpf)NULL,                        // INTTM01  Vect_2E
	(InterruptRoutine_tpf)NULL,                        // INTTM02  Vect_30
	(InterruptRoutine_tpf)r_tau0_channel3_interrupt,   // INTTM03  Vect_32
	(InterruptRoutine_tpf)NULL,                        // INTAD    Vect_34
	(InterruptRoutine_tpf)NULL,                        // INTRTC   Vect_36
	(InterruptRoutine_tpf)NULL,                        // INTIT    Vect_38
	(InterruptRoutine_tpf)NULL,                        // INTKR    Vect_3A
	(InterruptRoutine_tpf)NULL,                        // INTST3   Vect_3C
	(InterruptRoutine_tpf)NULL,                        // INTSR3   Vect_3E
	(InterruptRoutine_tpf)NULL,                        // INTTM13  Vect_40
	(InterruptRoutine_tpf)r_tau0_channel4_interrupt,   // INTTM04  Vect_42
	(InterruptRoutine_tpf)r_tau0_channel5_interrupt,   // INTTM05  Vect_44
	(InterruptRoutine_tpf)NULL,                        // INTTM06  Vect_46
	(InterruptRoutine_tpf)NULL,                        // INTTM07  Vect_48
	(InterruptRoutine_tpf)NULL,                        // INTP6    Vect_4A
	(InterruptRoutine_tpf)NULL,                        // INTP7    Vect_4C
	(InterruptRoutine_tpf)NULL,                        // INTP8    Vect_4E
	(InterruptRoutine_tpf)NULL,                        // INTP9    Vect_50
	(InterruptRoutine_tpf)NULL,                        // INTP10   Vect_52
	(InterruptRoutine_tpf)NULL,                        // INTP11   Vect_54
	(InterruptRoutine_tpf)NULL,                        // INTTM10  Vect_56
	(InterruptRoutine_tpf)NULL,                        // INTTM11  Vect_58
	(InterruptRoutine_tpf)NULL,                        // INTTM12  Vect_5A
	(InterruptRoutine_tpf)NULL,                        // INTSRE3  Vect_5C
	(InterruptRoutine_tpf)NULL,                        // INTMD    Vect_5E
	(InterruptRoutine_tpf)NULL,                        // INTIICA1 Vect_60
	(InterruptRoutine_tpf)NULL,                        // INTFL    Vect_62
	(InterruptRoutine_tpf)NULL,                        // INTDMA2  Vect_64
	(InterruptRoutine_tpf)NULL,                        // INTDMA3  Vect_66
	(InterruptRoutine_tpf)NULL,                        // INTTM14  Vect_68
	(InterruptRoutine_tpf)NULL,                        // INTTM15  Vect_6A
	(InterruptRoutine_tpf)NULL,                        // INTTM16  Vect_6C
	(InterruptRoutine_tpf)NULL,                        // INTTM17  Vect_6E
	(InterruptRoutine_tpf)NULL,                        // NULL     Vect_70
	(InterruptRoutine_tpf)NULL,                        // NULL     Vect_72
	(InterruptRoutine_tpf)NULL,                        // NULL     Vect_74
	(InterruptRoutine_tpf)NULL,                        // NULL     Vect_76
	(InterruptRoutine_tpf)NULL,                        // NULL     Vect_78
	(InterruptRoutine_tpf)NULL,                        // NULL     Vect_7A
	(InterruptRoutine_tpf)NULL,                        // NULL     Vect_7C
	(InterruptRoutine_tpf)NULL                         // BRK      Vect_7E
};

/* 函数声明 *******************************************************************/
/* 函数定义 *******************************************************************/


/***************************** 文件结束 ***************************************/
