/*******************************************************************************
  * 文件：ImageHeader.c
  * 作者：zyz
  * 版本：v1.0.0
  * 日期：2017-08-03
  * 说明：Image header
*******************************************************************************/

/* 头文件 *********************************************************************/
#include "ImageHeader.h"
#include "JumpFunction.h"
#include "main.h"

/* 宏定义 *********************************************************************/
/* 类型定义 *******************************************************************/
/* 变量定义 *******************************************************************/
// 中断向量表
extern __root const InterruptRoutine_tpf apfInteruptVectorTable[];

// 程序头信息
#pragma location=u32APPLICATION_HEADER_ADDRESS
__root static const ImageHeader_ts sImageHeader =
{
    (U8)eIMAGE_TYPE_APPLICATION,         // 程序类型
    (U8)u8SW_MAJOR_VERSION,              // 主版本
    (U8)u8SW_MINOR_VERSION,              // 次版本
    (U8)u8SW_REVISION_VERSION,           // 修订版本
    (U16)u16SW_EXTENDED_VERSION_YEAR,    // 发布年
    (U8)u8SW_EXTENDED_VERSION_MONTH,     // 发布月
    (U8)u8SW_EXTENDED_VERSION_DAY,       // 发布日
    strSW_ID,                            // 标识符
    (U32)u32IMAGE_ADDRESS,               // 程序地址
    (U32)u32IMAGE_SIZE,                  // 程序大小
    (U32)u32PARM_ADDRESS,                // 参数地址
    (U32)u32PARM_SIZE,                   // 参数大小
    (U32)apfInteruptVectorTable,         // 启动地址
    (U16)0x1234,                         // 数据校验
    (U16)0x5678                          // 头信息校验
};

/* 函数声明 *******************************************************************/
/* 函数定义 *******************************************************************/


/***************************** 文件结束 ***************************************/
